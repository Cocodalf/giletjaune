import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';

const MODULES = [
  CommonModule,
  IonicModule
];

@NgModule({
  imports: [
    ...MODULES,
  ],
  exports: [
    ...MODULES
  ]
})
export class SharedModule { }
