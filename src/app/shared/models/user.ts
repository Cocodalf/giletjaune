import { DocumentReference } from '@angular/fire/firestore';

export interface User {
  uid: string;
  displayName: string;
  email: string;
  phoneNumber: string;
  photoURL: string;
  friends: string[];
}
