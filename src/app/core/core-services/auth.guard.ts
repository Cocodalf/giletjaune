import { Injectable } from '@angular/core';

import { AuthService } from '../core-services/auth.service';
import { CanLoad } from '@angular/router';

@Injectable()
export class AuthGuard implements CanLoad {

  constructor(private authService: AuthService) {}

  canLoad(): boolean {
    return !!this.authService.currentUser;
  }

}
