import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { ToastOptions } from '@ionic/core';

@Injectable()
export class ToastService {


  options: ToastOptions = {
    duration: 2000,
    position: 'top',
    color: 'tertiary'
  };

  constructor(private toastCtrl: ToastController) { }

  async showGreetings(displayName: string) {
    const toast = await this.toastCtrl.create({
      message: `Hello ${displayName} !`,
      ...this.options
    });
    await toast.present();
  }
}
