import { Injectable } from '@angular/core';

import { Observable, from } from 'rxjs';

import * as firebase from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable()
export class AuthService {

  constructor(private afAuth: AngularFireAuth, private afs: AngularFirestore) {
    this.afAuth.authState.subscribe(authState => {
      if (authState) {
        this.afs.collection('users').doc(authState.uid).set(
          { email: authState.email, phoneNumber: authState.phoneNumber, status: 'online', friends: [] },
          { merge: true }
        );
      }
    });
  }

  get currentUser() {
    return this.afAuth.auth.currentUser;
  }

  load(): Observable<firebase.User> {
    return this.afAuth.authState;
  }

  signOut(): Observable<void> {
    return from(<Promise<void>>this.afAuth.auth.signOut());
  }

}
