import { Injectable } from '@angular/core';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';


@Injectable()
export class AndroidPermissionsService {

  permissions = {
    camera: this.androidPermissions.PERMISSION.CAMERA,
    recordAudio: this.androidPermissions.PERMISSION.RECORD_AUDIO,
    audioSettings: this.androidPermissions.PERMISSION.MODIFY_AUDIO_SETTINGS
  };

  constructor(private androidPermissions: AndroidPermissions) {}

  async checkAndroidPermissions(): Promise<void> {
    await this.androidPermissions.requestPermissions([
      this.permissions.camera,
      this.permissions.recordAudio,
      this.permissions.audioSettings,
    ]);
    const camera = await this.checkCameraPermission();
    const recordAudio = await this.checkRecordAudioPermission();
    const audioSettings = await this.checkAudioSettingsPermission();
    if (!camera.hasPermission || !recordAudio.hasPermission || audioSettings.hasPermission) {
      // error
    }
    // ok
  }

  private async checkCameraPermission() {
    return await this.androidPermissions.checkPermission(this.permissions.camera);
  }

  private async checkRecordAudioPermission() {
    return await this.androidPermissions.checkPermission(this.permissions.recordAudio);
  }

  private async checkAudioSettingsPermission() {
    return await this.androidPermissions.checkPermission(this.permissions.audioSettings);
  }
}
