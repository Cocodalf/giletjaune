import { RouterStateSerializer, BaseRouterStoreState } from '@ngrx/router-store';

export interface State extends RouterStateSerializer<BaseRouterStoreState> { }
