import { Injectable } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivationEnd } from '@angular/router';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { map, tap, filter } from 'rxjs/operators';

import * as actions from './actions';

@Injectable()
export class RouterEffects {
  constructor(
    private actions$: Actions,
    private router: Router,
    private location: Location,
    private store: Store<any>
  ) {
    this._litenToRouter();
  }

  @Effect({ dispatch: false })
  navigate$ = this.actions$.pipe(
    ofType(actions.Types.GO),
    map((action: actions.GoAction) => action.payload),
    tap(({ path, queryParams, extras }) => this.router.navigate(path, { queryParams, ...extras }))
  );

  @Effect({ dispatch: false })
  navigateBack$ = this.actions$.pipe(
    ofType(actions.Types.BACK),
    tap(() => this.location.back())
  );

  @Effect({ dispatch: false })
  navigateForward$ = this.actions$.pipe(
    ofType(actions.Types.FORWARD),
    tap(() => this.location.forward())
  );

  _litenToRouter() {
    this.router.events.pipe(filter(event => event instanceof ActivationEnd))
      .subscribe((event: ActivationEnd) => this.store.dispatch(
        new actions.ChangeAction({
          params: { ...event.snapshot.params },
          queryParams: { ...event.snapshot.queryParams },
          path: event.snapshot.routeConfig.path
        })
      ));
  }
}
