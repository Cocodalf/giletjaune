import { Action } from '@ngrx/store';
import { NavigationExtras } from '@angular/router';

export enum Types {
  GO = '[ROUTER] GO',
  BACK = '[ROUTER] BACK',
  FORWARD = '[ROUTER] FORWARD',
  CHANGE = '[ROUTER] CHANGE',
}

export class GoAction implements Action {
  readonly type = Types.GO;
  constructor(
    public payload: {
      path: any[],
      queryParams?: object,
      extras?: NavigationExtras
    }
  ) { }
}

export class BackAction implements Action {
  readonly type = Types.BACK;
}

export class ForwardAction implements Action {
  readonly type = Types.FORWARD;
}

export class ChangeAction implements Action {
  readonly type = Types.CHANGE;
  constructor(public payload: { params: any, queryParams: any, path: string }) { }
}

export type All =
  GoAction
  | BackAction
  | ForwardAction
  | ChangeAction;
