export interface State {
  uid: string;
  isAuthenticated: boolean;
  isLoaded: boolean;
  isLoading: boolean;
  error?: string;
}

export const initialState: State = {
  uid: null,
  isAuthenticated: false,
  isLoaded: false,
  isLoading: false
};
