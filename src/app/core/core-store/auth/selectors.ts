import { createFeatureSelector, createSelector, MemoizedSelector } from '@ngrx/store';
import { State } from './state';

const getUid = (state: State): string => state.uid;
const getIsAuthenticated = (state: State): boolean => state.isAuthenticated;
const getIsLoaded = (state: State): boolean => state.isLoaded;
const getIsLoading = (state: State): boolean => state.isLoading;
const getError = (state: State): string => state.error;

export const selectAuthState: MemoizedSelector<object, State> = createFeatureSelector('auth');
export const selectAuthUid: MemoizedSelector<object, string> = createSelector(selectAuthState, getUid);
export const selectAuthIsAuthenticated: MemoizedSelector<object, boolean> = createSelector(selectAuthState, getIsAuthenticated);
export const selectAuthIsLoaded: MemoizedSelector<object, boolean> = createSelector(selectAuthState, getIsLoaded);
export const selectIsLoading: MemoizedSelector<object, boolean> = createSelector(selectAuthState, getIsLoading);
export const selectAuthError: MemoizedSelector<object, string> = createSelector(selectAuthState, getError);
