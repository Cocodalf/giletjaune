import { Action } from '@ngrx/store';

export enum Types {
  LOAD = '[Auth] load',
  AUTHENTICATED = '[Auth] authenticated',
  NOT_AUTHENTICATED = '[Auth] not authenticated',
  SIGN_OUT = '[Auth] signout',
  ERROR = '[Auth] error',
}

export class LoadAction implements Action {
  readonly type = Types.LOAD;
}

export class SignOutAction implements Action {
  readonly type = Types.SIGN_OUT;
}

export class AuthenticatedAction implements Action {
  readonly type = Types.AUTHENTICATED;
  constructor(public payload: string ) { }
}

export class NotAuthenticatedAction implements Action {
  readonly type = Types.NOT_AUTHENTICATED;
}

export class ErrorAction implements Action {
  readonly type = Types.ERROR;
  constructor(public payload: string ) { }
}

export type All =
  LoadAction
  | SignOutAction
  | AuthenticatedAction
  | NotAuthenticatedAction
  | ErrorAction;
