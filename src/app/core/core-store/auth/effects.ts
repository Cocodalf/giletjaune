import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { Action } from '@ngrx/store';
import { switchMap, map, startWith, delay } from 'rxjs/operators';

import * as actions from './actions';

import { RouterActions } from 'src/app/core/core-store';
import { AuthService } from 'src/app/core/core-services/auth.service';

@Injectable()
export class AuthEffects {

  @Effect()
  load$: Observable<Action> = this.actions$.pipe(
    ofType<actions.LoadAction>(actions.Types.LOAD),
    startWith(new actions.LoadAction()),
    switchMap(() => this.authService.load()),
    map(auth => auth ? new actions.AuthenticatedAction(auth.uid) : new actions.NotAuthenticatedAction())
  );

  @Effect()
  signOut$: Observable<Action> = this.actions$.pipe(
    ofType<actions.SignOutAction>(actions.Types.SIGN_OUT),
    switchMap(() => this.authService.signOut()),
    map(() => new actions.NotAuthenticatedAction())
  );

  @Effect()
  authenticated$: Observable<Action> = this.actions$.pipe(
    ofType<actions.AuthenticatedAction>(actions.Types.AUTHENTICATED),
    map(() => new RouterActions.GoAction({ path: ['user'] }))
  );

  @Effect()
  notAuthenticated$: Observable<Action> = this.actions$.pipe(
    ofType<actions.NotAuthenticatedAction>(actions.Types.NOT_AUTHENTICATED),
    map(() => new RouterActions.GoAction({ path: ['guest'] }))
  );

  constructor(private actions$: Actions, private authService: AuthService) { }
}
