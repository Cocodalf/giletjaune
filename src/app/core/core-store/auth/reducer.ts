import { State, initialState } from './state';
import * as actions from './actions';

export function authReducer(state: State = initialState, action: actions.All) {
  switch (action.type) {
    case actions.Types.LOAD:
      return { ...state, isLoading: true };
    case actions.Types.AUTHENTICATED:
      return { ...state, isLoaded: true, isLoading: false, uid: action.payload, isAuthenticated: true };
    case actions.Types.NOT_AUTHENTICATED:
      return { ...state, isLoaded: true, isLoading: false, uid: null, isAuthenticated: false };
    case actions.Types.ERROR:
      return { ...state, error: action.payload };
    default:
      return state;
  }
}
