import { createSelector } from '@ngrx/store';
import { AuthSelectors } from './auth';

export const selectAppIsLoaded = createSelector(
  AuthSelectors.selectAuthIsLoaded,
  (authIsLoaded: boolean) => authIsLoaded
);
