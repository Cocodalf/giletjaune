import { RouterState } from './router';
import { AuthState } from './auth';

export interface State {
  router: RouterState.State;
  auth: AuthState.State;
}
