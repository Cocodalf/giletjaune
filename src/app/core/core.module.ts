import { NgModule } from '@angular/core';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';

import { CoreStoreModule } from './core-store/core-store.module';
import { AuthService } from './core-services/auth.service';
import { AndroidPermissionsService } from './core-services/android-permissions.service';
import { ToastService } from './core-services/toast.service';

@NgModule({
  imports: [
    CoreStoreModule
  ],
  providers: [
    AndroidPermissions,
    AndroidPermissionsService,
    ToastService,
    AuthService,
  ],
  exports: [
    CoreStoreModule
  ],
})
export class CoreModule { }
