import { Component } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';

import { CoreState, AuthActions } from 'src/app/core/core-store';
import {
  UserSelectors,
  ProfileActions,
  FriendActions,
  ConversationActions,
  RequestActions,
  ContactActions
} from './user-store';
import { User } from '../shared/models/user';

@Component({
  selector: 'app-user',
  templateUrl: './user.page.html',
  styleUrls: ['./user.page.scss'],
})
export class UserPage {


  profile$: Observable<User> = this.store$.pipe(select(UserSelectors.selectProfileState));

  constructor(private store$: Store<CoreState.State>) { }

  ionViewWillEnter() {
    this.store$.dispatch(new ProfileActions.LoadAction());
    this.store$.dispatch(new FriendActions.LoadAction());
    this.store$.dispatch(new ConversationActions.LoadAction());
    this.store$.dispatch(new RequestActions.LoadAction());
    this.store$.dispatch(new ContactActions.LoadAction());
  }

  signOut() {
    this.store$.dispatch(new AuthActions.SignOutAction());
  }

}
