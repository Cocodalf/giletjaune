import { Component, OnInit, Input } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { ModalController } from '@ionic/angular';
import { Observable } from 'rxjs';


import { CoreState, RouterActions } from 'src/app/core/core-store';
import { Conversation } from '../user-shared/models/conversation';
import { UserSelectors, ConversationActions, ProfileActions } from '../user-store';
import { CreateGroupModalComponent } from './components/create-group-modal/create-group-modal.component';
import { UserStatusModalComponent } from './components/user-status-modal/user-status-modal.component';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  @Input() menuId: string;
  @Input() contentId: string;
  isGroup: boolean;
  groups$: Observable<Conversation[]> = this.store$.pipe(select(UserSelectors.selectAllPublicConversations));
  selectedGroupId$: Observable<string> = this.store$.pipe(select(UserSelectors.selectSelectedConversation));

  constructor(private store$: Store<CoreState.State>, private modalCtrl: ModalController) { }

  ngOnInit() {
  }

  handleFriendPanelSelect() {
    this.isGroup = false;
  }

  handleGroupSelect(group) {
    this.isGroup = true;
    this.store$.dispatch(new ConversationActions.SelectAction(group.uid));
  }

  async handleAddGroup() {
    const modal = await this.modalCtrl.create({
      component: CreateGroupModalComponent,
    });
    await modal.present();
    const { data } = await modal.onDidDismiss();
    if (data) {
      this.store$.dispatch(new ConversationActions.CreatePublicAction(data));
    }
  }

  goToProfilePage() {
    this.store$.dispatch(new RouterActions.GoAction({ path: ['user', 'profile'] }));
  }

  async openStatusModal() {
    const modal = await this.modalCtrl.create({ component: UserStatusModalComponent });
    await modal.present();
    const { data } = await modal.onDidDismiss();
    if (data) {
      this.store$.dispatch(new ProfileActions.UpdateStatusAction(data.status));
    }
  }

}
