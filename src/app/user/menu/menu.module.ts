import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MenuComponent } from './menu.component';
import { FriendButtonComponent } from './components/friend-button/friend-button.component';
import { AddGroupButtonComponent } from './components/add-group-button/add-group-button.component';
import { ConversationPanelComponent } from './components/conversation-panel/conversation-panel.component';
import { UserStatusComponent } from './components/user-status/user-status.component';
import { UserSharedModule } from '../user-shared/shared.module';
import { GroupButtonComponent } from './components/group-button/group-button.component';
import { GroupPanelComponent } from './components/group-panel/group-panel.component';
import { CreateGroupModalComponent } from './components/create-group-modal/create-group-modal.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AddFriendModalComponent } from './components/add-friend-modal/add-friend-modal.component';
import { UserStatusModalComponent } from './components/user-status-modal/user-status-modal.component';

@NgModule({
  imports: [
    UserSharedModule,
    RouterModule,
    ReactiveFormsModule
  ],
  entryComponents: [
    CreateGroupModalComponent,
    AddFriendModalComponent,
    UserStatusModalComponent
  ],
  declarations: [
    MenuComponent,
    FriendButtonComponent,
    GroupButtonComponent,
    AddGroupButtonComponent,
    ConversationPanelComponent,
    GroupPanelComponent,
    UserStatusComponent,
    CreateGroupModalComponent,
    AddFriendModalComponent,
    UserStatusModalComponent
  ],
  exports: [
    MenuComponent,
  ]
})
export class MenuModule { }
