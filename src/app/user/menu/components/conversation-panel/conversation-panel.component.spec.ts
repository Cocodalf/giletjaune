import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConversationPanelPage } from './conversation-panel.page';

describe('ConversationPanelPage', () => {
  let component: ConversationPanelPage;
  let fixture: ComponentFixture<ConversationPanelPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConversationPanelPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConversationPanelPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
