import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Conversation } from '../../../user-shared/models/conversation';
import { Store, select } from '@ngrx/store';
import { UserState, UserSelectors } from '../../../user-store';
import { RouterActions, AuthSelectors } from 'src/app/core/core-store';

@Component({
  selector: 'app-conversation-panel',
  templateUrl: './conversation-panel.component.html',
  styleUrls: ['./conversation-panel.component.scss'],
})
export class ConversationPanelComponent implements OnInit {

  conversations$: Observable<Conversation[]> = this.store$.pipe(select(UserSelectors.selectAllPrivateConversations));
  authId$: Observable<string> = this.store$.pipe(select(AuthSelectors.selectAuthUid));

  constructor(private store$: Store<UserState.State>) { }

  ngOnInit() {
  }

  goToConversation(conversation: Conversation) {
    this.store$.dispatch(new RouterActions.GoAction({ path: [ 'user', 'conversation', conversation.uid ] }));
  }

  goToConferencePage() {
    this.store$.dispatch(new RouterActions.GoAction({ path: ['user', 'conference', 'my-first-conf'] }));
  }
}
