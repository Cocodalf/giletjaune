import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FriendButtonPage } from './friend-button.page';

describe('FriendButtonPage', () => {
  let component: FriendButtonPage;
  let fixture: ComponentFixture<FriendButtonPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FriendButtonPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FriendButtonPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
