import { Component, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-friend-button',
  templateUrl: './friend-button.component.html',
  styleUrls: ['./friend-button.component.scss'],
})
export class FriendButtonComponent {

  @Input() isActive: boolean;
  @Output() select: EventEmitter<boolean> = new EventEmitter();
  constructor() { }

  handleSelect($event) {
    this.select.emit($event);
  }

}
