import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-user-status-modal',
  templateUrl: './user-status-modal.component.html',
  styleUrls: ['./user-status-modal.component.scss'],
})
export class UserStatusModalComponent implements OnInit {

  constructor(private modalCtrl: ModalController) { }

  ngOnInit() {}

  close() {
    this.modalCtrl.dismiss();
  }

  handleStatusSelect(status: string) {
    this.modalCtrl.dismiss({ status });
  }
}
