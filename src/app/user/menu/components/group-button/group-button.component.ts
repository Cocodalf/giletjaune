import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Conversation } from 'src/app/user/user-shared/models/conversation';

@Component({
  selector: 'app-group-button',
  templateUrl: './group-button.component.html',
  styleUrls: ['./group-button.component.scss'],
})
export class GroupButtonComponent implements OnInit {

  @Input() isActive: boolean;
  @Input() conversation: Conversation;
  @Output() select: EventEmitter<any> = new EventEmitter();
  constructor() { }

  ngOnInit() {}

  handleSelect($event) {
    this.select.emit($event);
  }

}
