import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupButtonPage } from './group-button.page';

describe('GroupButtonPage', () => {
  let component: GroupButtonPage;
  let fixture: ComponentFixture<GroupButtonPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupButtonPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupButtonPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
