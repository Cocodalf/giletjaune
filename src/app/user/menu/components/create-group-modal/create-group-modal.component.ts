import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { UserState, UserSelectors } from 'src/app/user/user-store';
import { Observable } from 'rxjs';
import { User } from 'src/app/shared/models/user';
import { ModalController } from '@ionic/angular';
import { FormGroup, FormControl, Validators, ValidatorFn, FormArray } from '@angular/forms';


export function minSelectedCheckboxes(min = 1) {
  const validator: ValidatorFn = (formArray: FormArray) => {
    const totalSelected = formArray.controls
      .map(control => control.value)
      .reduce((prev, next) => next ? prev + next : prev, 0);
    return totalSelected >= min ? null : { required: true };
  };

  return validator;
}

@Component({
  selector: 'app-create-group-modal',
  templateUrl: './create-group-modal.component.html',
  styleUrls: ['./create-group-modal.component.scss'],
})
export class CreateGroupModalComponent implements OnInit {

  friends$: Observable<User[]> = this.store$.pipe(select(UserSelectors.selectAllFriends));
  friends: User[];
  groupForm: FormGroup = new FormGroup({
    displayName: new FormControl('', [
      Validators.required,
      Validators.minLength(4),
    ]),
    friendSelect: new FormArray([], [
      minSelectedCheckboxes(1)
    ])
  });

  constructor(private store$: Store<UserState.State>, private modalCtrl: ModalController) { }

  ngOnInit() {
    this.friends$.subscribe(friends => {
      this.friends = friends;
      this.addCheckboxes();
    });
  }

  close() {
    this.modalCtrl.dismiss();
  }

  handleSubmit() {
    this.modalCtrl.dismiss({ displayName: this.groupForm.controls.displayName.value, friendIds: this.getIds() });
  }

  private getIds() {
    return  this.friends.filter((friend, i) => this.groupForm.controls.friendSelect.value[i])
      .map(friend => friend.uid);
  }

  private addCheckboxes() {
    this.friends.forEach(() => (this.groupForm.controls.friendSelect as FormArray).push(new FormControl()));
  }

}
