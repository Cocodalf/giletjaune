import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { UserState, UserSelectors, RequestActions, RequestSelectors, ConversationActions } from 'src/app/user/user-store';
import { Observable, of } from 'rxjs';
import { Conversation } from 'src/app/user/user-shared/models/conversation';
import { switchMap, map, withLatestFrom, mergeMap, concatMap, filter } from 'rxjs/operators';
import { AuthSelectors } from 'src/app/core/core-store';
import { User } from 'src/app/shared/models/user';
import { Request } from 'src/app/user/user-shared/models/request';
import { ModalController } from '@ionic/angular';
import { AddFriendModalComponent } from '../add-friend-modal/add-friend-modal.component';

@Component({
  selector: 'app-group-panel',
  templateUrl: './group-panel.component.html',
  styleUrls: ['./group-panel.component.scss'],
})
export class GroupPanelComponent implements OnInit {

  group: Conversation;
  authId$: Observable<string> = this.store$.pipe(select(AuthSelectors.selectAuthUid));
  authId: string;
  selectedGroupId$: Observable<string> = this.store$.pipe(select(UserSelectors.selectSelectedConversation));
  friends$: Observable<User[]> = this.store$.pipe(select(UserSelectors.selectAllFriends));
  requests$: Observable<Request[]> = this.store$.pipe(select(UserSelectors.selectAllRequests));
  selectedGroup$: Observable<Conversation> = this.selectedGroupId$.pipe(
    switchMap(selected => selected
      ? this.store$.pipe(select(UserSelectors.selectPublicConversationById, selected))
      : of(null)
    )
  );

  constructor(private store$: Store<UserState.State>, private modalCtrl: ModalController) { }

  ngOnInit() {
    this.authId$.subscribe(authId => this.authId = authId);
    this.selectedGroup$.subscribe(group => this.group = group);
  }

  displaySend(member: User) {
    return this.authId$.pipe(
      switchMap(authId => this.friends$.pipe(
        withLatestFrom(this.requests$),
        map(([friends, requests]) => {
          if (authId === member.uid) {
            return false;
          }
          const isFriend = friends.some(friend => friend.uid === member.uid);
          const isRequested = requests.some(request => request.members.includes(member.uid));
          return !isFriend && !isRequested;
        })
      )
    ));
  }

  invit(member: User): void {
    this.store$.dispatch(new RequestActions.CreateAction(member.uid));
  }

  delete(member: User): void {
    const filteredGroupMembers = this.group.members.filter(groupMember => groupMember !== member).map(groupMember => groupMember.uid);
    const members = {};
    filteredGroupMembers.forEach(id => members[id] = true);
    this.store$.dispatch(new ConversationActions.UpdateAction({ ...this.group, members }));
  }

  async addMembers() {
    const modal = await this.modalCtrl.create({ component: AddFriendModalComponent });
    await modal.present();
    const { data } = await modal.onDidDismiss();
    if (data) {
      const members = {};
      members[this.authId] = true;
      data.friendIds.forEach(id => members[id] = true);
      this.store$.dispatch(new ConversationActions.UpdateAction({...this.group, members}));
    }
  }

}
