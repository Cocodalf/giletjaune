import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { UserState, UserSelectors } from 'src/app/user/user-store';
import { Observable } from 'rxjs';
import { User } from 'src/app/shared/models/user';

@Component({
  selector: 'app-user-status',
  templateUrl: './user-status.component.html',
  styleUrls: ['./user-status.component.scss'],
})
export class UserStatusComponent implements OnInit {

  profile$: Observable<User> = this.store$.pipe(select(UserSelectors.selectProfileState));

  constructor(private store$: Store<UserState.State>) { }

  ngOnInit() {}

}
