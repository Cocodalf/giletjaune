import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ValidatorFn, FormArray, FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { User } from 'src/app/shared/models/user';
import { Store, select } from '@ngrx/store';
import { UserState, UserSelectors } from 'src/app/user/user-store';


export function minSelectedCheckboxes(min = 1) {
  const validator: ValidatorFn = (formArray: FormArray) => {
    const totalSelected = formArray.controls
      .map(control => control.value)
      .reduce((prev, next) => next ? prev + next : prev, 0);
    return totalSelected >= min ? null : { required: true };
  };

  return validator;
}

/*
  TODO filter for members
*/
@Component({
  selector: 'app-add-friend-modal',
  templateUrl: './add-friend-modal.component.html',
  styleUrls: ['./add-friend-modal.component.scss'],
})
export class AddFriendModalComponent implements OnInit {
  friends$: Observable<User[]> = this.store$.pipe(select(UserSelectors.selectAllFriends));
  friends: User[];
  groupForm: FormGroup = new FormGroup({
    friendSelect: new FormArray([], [
      minSelectedCheckboxes(1)
    ])
  });

  constructor(private store$: Store<UserState.State>, private modalCtrl: ModalController) { }

  ngOnInit() {
    this.friends$.subscribe(friends => {
      this.friends = friends;
      this.addCheckboxes();
    });
  }

  handleSubmit() {
    this.modalCtrl.dismiss({ friendIds: this.getIds() });
  }

  close() {
    this.modalCtrl.dismiss();
  }

  private getIds() {
    return  this.friends.filter((friend, i) => this.groupForm.controls.friendSelect.value[i])
      .map(friend => friend.uid);
  }

  private addCheckboxes() {
    this.friends.forEach(() => (this.groupForm.controls.friendSelect as FormArray).push(new FormControl()));
  }

}
