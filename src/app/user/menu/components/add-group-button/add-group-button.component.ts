import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-add-group-button',
  templateUrl: './add-group-button.component.html',
  styleUrls: ['./add-group-button.component.scss'],
})
export class AddGroupButtonComponent {

  @Output() add: EventEmitter<any> = new EventEmitter();

  constructor() { }

  handleAddClick($event) {
    this.add.emit($event);
  }

}
