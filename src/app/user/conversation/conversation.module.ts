import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { ConversationPage } from './conversation.page';
import { MessageComponent } from './message/message.component';
import { UserSharedModule } from '../user-shared/shared.module';

const routes: Routes = [
  {
    path: '',
    component: ConversationPage
  }
];

@NgModule({
  imports: [
    UserSharedModule,
    FormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    ConversationPage,
    MessageComponent,
  ]
})
export class ConversationPageModule {}
