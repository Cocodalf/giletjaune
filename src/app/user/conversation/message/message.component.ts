import { Component, OnInit, Input, Directive, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import * as marked from 'marked';

import { User } from 'src/app/shared/models/user';
import { UserState, UserSelectors } from '../../user-store';
import { Message } from '../../user-shared/models/message';


@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss'],
})
export class MessageComponent implements OnInit, AfterViewInit {

  author: Partial<User>;
  @Input() message: Message;
  @Input() members: User[];
  @ViewChild('content') content: ElementRef;

  constructor(private store$: Store<UserState.State>) { }

  ngOnInit() {
    this.store$.pipe(select(UserSelectors.selectProfileState)).subscribe((profile) => {
      if (this.message.author === profile.uid) {
        this.author = { ...profile };
      } else {
        this.author = this.members.find(member => member.uid === this.message.author);
      }
    });
  }

  ngAfterViewInit() {
    this.content.nativeElement.innerHTML = `<ion-text color="medium">${marked(this.message.content)}</ion-text>`;
  }
}
