import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';

import { UserState, UserSelectors, MessageActions, ConversationActions } from '../user-store';
import { Message } from '../user-shared/models/message';
import { Conversation } from '../user-shared/models/conversation';
@Component({
  selector: 'app-conversation',
  templateUrl: './conversation.page.html',
  styleUrls: ['./conversation.page.scss'],
})
export class ConversationPage {

  message: string;
  messages$: Observable<Message[]> = this.store$.pipe(select(UserSelectors.selectAllMessages));
  conversation$: Observable<Conversation> =
    this.store$.pipe(select(UserSelectors.selectConversationById, this.route.snapshot.params['conversationId']));
  constructor(
    private store$: Store<UserState.State>,
    private route: ActivatedRoute,
  ) {}

  ionViewWillEnter() {
    this.store$.dispatch(new ConversationActions.SelectAction(this.route.snapshot.params['conversationId']));
  }

  sendMessage(): void {
    if (!this.message) { return; }
    this.store$.dispatch(new MessageActions.CreateAction({
      conversationId: this.route.snapshot.params['conversationId'],
      content: this.message
    }));
    this.message = '';
  }

}
