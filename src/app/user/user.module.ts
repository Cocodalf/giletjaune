import { NgModule } from '@angular/core';
import { File } from '@ionic-native/file/ngx';

import { UserPage } from './user.page';

import { SharedModule } from '../shared/shared.module';
import { UserRoutingModule } from './user-routing.module';
import { UserStoreModule } from './user-store/user-store.module';
import { MenuModule } from './menu/menu.module';

@NgModule({
  imports: [
    SharedModule,
    UserRoutingModule,
    UserStoreModule,
    MenuModule
  ],
  declarations: [UserPage],
  providers: [File]
})
export class UserPageModule {}
