import { NgModule } from '@angular/core';

import { DashboardPage } from './dashboard.page';
import { RequestItemComponent } from './components/request-item/request-item.component';
import { UserSharedModule } from '../user-shared/shared.module';
import { DashboardRoutingModule } from './dashboard-routing.module';

@NgModule({
  imports: [
    UserSharedModule,
    DashboardRoutingModule
  ],
  declarations: [
    DashboardPage,
    RequestItemComponent,
  ]
})
export class DashboardPageModule {}
