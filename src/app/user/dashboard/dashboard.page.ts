import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';

import { Observable } from 'rxjs';

import { User } from 'src/app/shared/models/user';
import { Request } from '../user-shared/models/request';
import { UserSelectors, UserState, FriendActions, RequestActions, ConversationActions } from '../user-store';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  segment = 'friend';
  friends$: Observable<User[]> = this.store$.pipe(select(UserSelectors.selectAllFriends));
  requestsFromAuth$: Observable<Request[]> = this.store$.pipe(select(UserSelectors.selectAllRequestsFromAuth));
  requestsForAuth$: Observable<Request[]> = this.store$.pipe(select(UserSelectors.selectAllRequestsForAuth));
  contacts$: Observable<User[]> = this.store$.pipe(select(UserSelectors.selectAllContacts));

  constructor(private store$: Store<UserState.State>) { }

  ngOnInit() {
  }

  segmentChanged(ev: any) {
    this.segment = ev.target.value;
  }

  delete(friend: User) {
    this.store$.dispatch(new FriendActions.DeleteAction(friend.uid));
  }

  privateTalk(friend: User) {
    this.store$.dispatch(new ConversationActions.CreatePrivateAction(friend.uid));
  }

  deleteRequest(request: Request): void {
    this.store$.dispatch(new RequestActions.DeleteAction(request.uid));
  }

  acceptRequest(request: Request): void {
    this.store$.dispatch(new FriendActions.CreateAction({ requestId: request.uid, friendId: request.target.uid }));
  }

  invit(contact: User): void {
    this.store$.dispatch(new RequestActions.CreateAction(contact.uid));
  }
}
