import { Component, Input, Output, EventEmitter } from '@angular/core';

import { Request } from '../../../user-shared/models/request';

@Component({
  selector: 'app-request-item',
  templateUrl: './request-item.component.html',
  styleUrls: ['./request-item.component.scss'],
})
export class RequestItemComponent {

  @Input() isFromAuth: boolean;
  @Input() request: Request;
  @Output() delete: EventEmitter<string> = new EventEmitter();
  @Output() accept: EventEmitter<string> = new EventEmitter();

  handleDelete() {
    this.delete.emit(this.request.uid);
  }

  handleAccept() {
    this.accept.emit(this.request.uid);
  }

}
