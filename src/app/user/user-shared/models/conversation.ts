import { Message } from './message';
import { User } from 'src/app/shared/models/user';

export enum ConversationTypes {
  PRIVATE = 'private',
  PUBLIC = 'public'
}
export interface Conversation {
  uid: string;
  members: User[];
  createdAt: Date;
  lastMessage: Message;
  type: ConversationTypes;
  photoURL?: string;
}
