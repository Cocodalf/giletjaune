import { User } from 'src/app/shared/models/user';

export interface Request {
  uid: string;
  members: string[];
  owner: string;
  createdAt: Date;
  acceptedAt: Date;
  target?: User;
}

