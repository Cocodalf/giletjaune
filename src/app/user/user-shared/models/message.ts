export interface Message {
  uid: string;
  content: string;
  author: string;
  createdAt: Date;
}

