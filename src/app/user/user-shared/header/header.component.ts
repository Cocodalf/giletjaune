import { Component, Input } from '@angular/core';
import { Store, select } from '@ngrx/store';

import { SignOutAction } from 'src/app/core/core-store/auth/actions';
import { UserState, UserSelectors } from '../../user-store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {

  userIsLoading$: Observable<boolean> = this.store$.pipe(select(UserSelectors.selectUserIsLoading));

  @Input() title: string;
  @Input() signOutButton: boolean;
  @Input() menuButton = true;

  constructor(private store$: Store<UserState.State>) { }

  signOut() {
    this.store$.dispatch(new SignOutAction());
  }

}
