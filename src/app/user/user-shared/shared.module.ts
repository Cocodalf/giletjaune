import { NgModule } from '@angular/core';

import { HeaderComponent } from './header/header.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { NoResultComponent } from './no-result/no-result.component';

const MODULES = [
  SharedModule
];

const COMPONENTS = [
  HeaderComponent,
  NoResultComponent
];

@NgModule({
  imports: [
    ...MODULES,
  ],
  declarations: [
    ...COMPONENTS
  ],
  exports: [
    ...MODULES,
    ...COMPONENTS
  ]
})
export class UserSharedModule { }
