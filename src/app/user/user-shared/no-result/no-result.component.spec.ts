import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NoResultPage } from './no-result.page';

describe('NoResultPage', () => {
  let component: NoResultPage;
  let fixture: ComponentFixture<NoResultPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NoResultPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NoResultPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
