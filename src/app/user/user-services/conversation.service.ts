import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentChangeAction } from '@angular/fire/firestore';

import { mergeMap, switchMap, map, concatMap } from 'rxjs/operators';
import { Observable, of, from } from 'rxjs';
import { Conversation, ConversationTypes } from '../user-shared/models/conversation';
import { pipe } from '@angular/core/src/render3';

@Injectable()
export class ConversationService {

  constructor(private afs: AngularFirestore) { }

  load(authId: string): any {
    return this.afs.collection<Conversation>(
        'conversations',
        ref => ref
        .where(`members.${authId}`, '==', true)
      )
      .stateChanges().pipe(
        mergeMap(docChanges => docChanges),
        concatMap(action => {
          const membersIds = Object.keys(action.payload.doc.data().members)
            .map(member => this.afs.collection('users').doc(member).get().toPromise());
            return from(Promise.all(membersIds)).pipe(
              map(members => ({ action, members: members.map(member => ({ ...member.data(), uid: member.id })) }))
            );
        })
      );

  }

  async createPrivate(friendId, authId): Promise<string> {
    const { docs } = await this.afs.collection(
      'conversations',
      ref => ref
      .where(`members.${friendId}`, '==', true)
      .where(`members.${authId}`, '==', true)
      .where('type', '==', ConversationTypes.PRIVATE)
    ).get().toPromise();
    if (docs.length) {
      return docs[0].id;
    }
    const conversationRef = await this.afs.collection('conversations')
      .add({ members: { [friendId]: true, [authId]: true }, createdAt: new Date(), type: ConversationTypes.PRIVATE });
    return conversationRef.id;
  }

  async createPublic({ displayName, friendIds }: { displayName: string, friendIds: string[] }, authId: string): Promise<string> {
    const members = {};
    members[authId] = true;
    friendIds.forEach(id => members[id] = true);
    const conversationRef = await this.afs.collection('conversations')
      .add({
        displayName,
        members,
        createdAt: new Date(),
        owner: authId, type: ConversationTypes.PUBLIC
      });
    return conversationRef.id;
  }

  async getByUsers(usersIds: string[]) {
    const { docs } = await this.afs.collection(
      'conversations',
      ref => ref.where(`members.${usersIds[0]}`, '==', true)
        .where(`members.${usersIds[1]}`, '==', true)
        .where('type', '==', 'private')
    ).get().toPromise();

    if (docs.length) {
      return { ...docs[0].data(), uid: docs[0].id };
    }
    return null;
  }

  async delete(conversationId): Promise<void> {
    return await this.afs.collection('conversations').doc(conversationId).delete();
  }

  async update(conversation: Partial<Conversation>) {
    const docRef = this.afs.collection('conversations').doc(conversation.uid);
    delete conversation.uid;
    return await docRef.update({ ...conversation });
  }

}
