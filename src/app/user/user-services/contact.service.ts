import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentChangeAction } from '@angular/fire/firestore';

import { mergeMap, filter } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { User } from 'src/app/shared/models/user';

@Injectable()
export class ContactService {

  constructor(private afs: AngularFirestore) { }

  load(authId: string): Observable<DocumentChangeAction<User>> {
    return this.afs.collection<User>('users').stateChanges().pipe(
      mergeMap(docChanges => docChanges),
      filter(changeAction => changeAction.payload.doc.id !== authId)
    );
  }

}
