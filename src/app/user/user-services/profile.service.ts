import { Injectable } from '@angular/core';
import { User } from 'src/app/shared/models/user';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable()
export class ProfileService {

  constructor(private afs: AngularFirestore) { }

  load(authId) {
    return this.afs.collection('users').doc<User>(authId).snapshotChanges();
  }

  async getProfileById(uid: string): Promise<any> {
    return await this.afs.collection<User>('users').doc(uid).get().toPromise();
  }

  async updateDisplayName(authId: string, displayName: string) {
    return await this.afs.collection<User>('users').doc(authId).set({ displayName: displayName }, { merge: true });
  }

  async updateStatus(authId: string, status: string) {
    return await this.afs.collection<User>('users').doc(authId).set({ status }, { merge: true });
  }
}
