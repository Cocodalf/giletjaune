import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

import { mergeMap } from 'rxjs/operators';

@Injectable()
export class FriendService {

  constructor(private afs: AngularFirestore) { }

  load(authId) {
    return this.afs.collection('users', ref => ref.where('friends', 'array-contains', authId)).stateChanges().pipe(
      mergeMap(docChanges => docChanges),
    );
  }

  async create(requestId: string, friendId: string, authId: string): Promise<string> {
    const authRef = this.afs.collection('users').doc(authId).ref;
    const friendRef = this.afs.collection('users').doc(friendId).ref;
    const [authDoc, friendDoc]: firebase.firestore.DocumentSnapshot[] = await Promise.all([
      authRef.get(),
      friendRef.get()
    ]);
    const authFriends = authDoc.data().friends;
    const friendFriends = friendDoc.data().friends;
    await Promise.all([
      authRef.set({ friends: [...authFriends, friendId] }, {  merge: true }),
      friendRef.set({ friends: [...friendFriends, authId] }, { merge: true })
    ]);
    return requestId;
  }

  async delete(friendId, authId): Promise<[string, string]> {
    const authRef = this.afs.collection('users').doc(authId).ref;
    const friendRef = this.afs.collection('users').doc(friendId).ref;
    const [authDoc, friendDoc]: firebase.firestore.DocumentSnapshot[] = await Promise.all([
      authRef.get(),
      friendRef.get()
    ]);
    const authFriends = authDoc.data().friends;
    const friendFriends = friendDoc.data().friends;
    await Promise.all([
      authRef.set({ friends: authFriends.filter(friend => friend !== friendRef.id) }, { merge: true }),
      friendRef.set({ friends: friendFriends.filter(friend => friend !== authRef.id) }, { merge: true })
    ]);
    return [friendId, authId];
  }
}
