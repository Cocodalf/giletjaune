import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

import { mergeMap, map, switchMap, concatMap } from 'rxjs/operators';

import { Request } from '../user-shared/models/request';
import { of } from 'rxjs';

@Injectable()
export class RequestService {

  constructor(private afs: AngularFirestore) { }

  load(authId: string) {
    return this.afs.collection<Request>(
      'requests',
      ref => ref.where('members', 'array-contains', authId)
    ).stateChanges().pipe(
      mergeMap(docChanges => docChanges),
      concatMap(action => {
        const members = action.payload.doc.data().members;
        const [targetId] = members.filter(member => member !== authId);
        return this.afs.collection('users').doc(targetId).get().pipe(
          switchMap(snapshot => of(({action,  target: {uid: snapshot.id,  ...snapshot.data()}})))
        );
      })
    );
  }

  async create(authId: string, friendId: string): Promise<string > {

    const { id } = await this.afs.collection('requests').add({
      members: [authId, friendId],
      owner: authId,
      createdAt: new Date()
    });
    return id;
  }

  delete(requestId: string ) {
    return this.afs.collection('requests').doc(requestId).delete();
  }

}
