import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Platform } from '@ionic/angular';
import { OpenVidu, Session, StreamManager, Subscriber, StreamEvent, Publisher } from 'openvidu-browser';

import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AndroidPermissionsService } from 'src/app/core/core-services/android-permissions.service';
import { User } from 'src/app/shared/models/user';

@Injectable()
export class OpenViduService {

  OPENVIDU_SERVER_URL = 'https://demos.openvidu.io:4443';
  OPENVIDU_SERVER_SECRET = 'MY_SECRET';

  mySessionId = 'ok';

  OV: OpenVidu;
  session: Session;
  subscribers: StreamManager[] = [];
  publisher: StreamManager;

  constructor(
    private platform: Platform,
    private httpClient: HttpClient,
    private androidPermissionsService: AndroidPermissionsService
  ) { }

  async join(profile: User, sessionId?: string) {
    this.OV = new OpenVidu();
    this.session = this.OV.initSession();
    this.session.on('streamCreated', (event: StreamEvent) => {
      const subscriber: Subscriber = this.session.subscribe(event.stream, undefined);
      this.subscribers.push(subscriber);
    });
    this.session.on('streamDestroyed', (event: StreamEvent) => {
    this.deleteSubscriber(event.stream.streamManager);
    });
    const token = await this.getToken(sessionId);
    await this.session.connect(token, { clientData: {...profile} });
    if (this.platform.is('cordova') && this.platform.is('android')) {
      await this.androidPermissionsService.checkAndroidPermissions();
      this.initPublisher();
    } else {
      this.initPublisher();
    }
  }

  leave() {
    if (this.session) { this.session.disconnect(); }
    this.subscribers = [];
    delete this.publisher;
    delete this.session;
    delete this.OV;
  }

  private deleteSubscriber(streamManger: StreamManager): void {
    const index = this.subscribers.indexOf(streamManger, 0);
    if (index > -1) {
      this.subscribers.splice(index, 1);
    }
  }

  private initPublisher() {
    const publisher: Publisher = this.OV.initPublisher(undefined, {
      audioSource: undefined,
      videoSource: undefined,
      publishAudio: true,
      publishVideo: true,
      resolution: '640x480',
      frameRate: 30,
      insertMode: 'APPEND',
      mirror: true
    });
    this.session.publish(publisher).then(() => {
      this.publisher = publisher;
    });
  }

  /*
     * --------------------------
     * SERVER-SIDE RESPONSIBILITY
     * --------------------------
     * This method retrieve the mandatory user token from OpenVidu Server,
     * in this case making use Angular http API.
     * This behaviour MUST BE IN YOUR SERVER-SIDE IN PRODUCTION. In this case:
     *   1) Initialize a session in OpenVidu Server	 (POST /api/sessions)
     *   2) Generate a token in OpenVidu Server		   (POST /api/tokens)
     *   3) The token must be consumed in Session.connect() method of OpenVidu Browser
     */

  getToken(sessionId?: string): Promise<string> {
    if (this.platform.is('ios') && this.platform.is('cordova') && this.OPENVIDU_SERVER_URL === 'https://localhost:4443') {
      // To make easier first steps with iOS apps, use demos OpenVidu Sever if no custom valid server is configured
      this.OPENVIDU_SERVER_URL = 'https://demos.openvidu.io:4443';
    }
    return this.createSession(sessionId).then((id) => {
      return this.createToken(id);
    });
  }

  createSession(sessionId: string) {
    return new Promise((resolve, reject) => {
      const body = JSON.stringify({ customSessionId: sessionId });
      const options = {
        headers: new HttpHeaders({
          Authorization: 'Basic ' + btoa('OPENVIDUAPP:' + this.OPENVIDU_SERVER_SECRET),
          'Content-Type': 'application/json',
        }),
      };
      return this.httpClient
        .post(this.OPENVIDU_SERVER_URL + '/api/sessions', body, options)
        .pipe(
          catchError((error) => {
            if (error.status === 409) {
              resolve(sessionId);
            } else {
              console.warn(
                'No connection to OpenVidu Server. This may be a certificate error at ' +
                this.OPENVIDU_SERVER_URL,
              );
              if (
                window.confirm(
                  'No connection to OpenVidu Server. This may be a certificate error at "' +
                  this.OPENVIDU_SERVER_URL +
                  // tslint:disable-next-line:max-line-length
                  '"\n\nClick OK to navigate and accept it. If no certificate warning is shown, then check that your OpenVidu Server' +
                  'is up and running at "' +
                  this.OPENVIDU_SERVER_URL +
                  '"',
                )
              ) {
                location.assign(this.OPENVIDU_SERVER_URL + '/accept-certificate');
              }
            }
            return throwError(error);
          }),
        )
        .subscribe((response) => {
          resolve(response['id']);
        });
    });
  }

  createToken(sessionId): Promise<string> {
    return new Promise((resolve, reject) => {
      const body = JSON.stringify({ session: sessionId });
      const options = {
        headers: new HttpHeaders({
          Authorization: 'Basic ' + btoa('OPENVIDUAPP:' + this.OPENVIDU_SERVER_SECRET),
          'Content-Type': 'application/json',
        }),
      };
      return this.httpClient
        .post(this.OPENVIDU_SERVER_URL + '/api/tokens', body, options)
        .pipe(
          catchError((error) => {
            reject(error);
            return throwError(error);
          }),
        )
        .subscribe((response) => {
          resolve(response['token']);
        });
    });
  }
}
