import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentChangeAction } from '@angular/fire/firestore';
import { mergeMap } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { Message } from '../user-shared/models/message';
import { Conversation } from '../user-shared/models/conversation';

@Injectable()
export class MessageService {

  constructor(private afs: AngularFirestore) { }

  load(conversationId: string): Observable<DocumentChangeAction<Message>> {
    return this.afs
      .collection<Conversation>('conversations').doc(conversationId)
      .collection<Message>('messages', ref => ref.orderBy('createdAt')).stateChanges()
      .pipe(mergeMap(docChanges => docChanges));
  }

  create(authId: string, conversationId: string, content: string) {
    const messageData = {
      author: authId,
      content,
      createdAt: new Date()
    };
    this.afs.collection('conversations').doc(conversationId).update({ lastMessage: messageData });
    return this.afs
      .collection<Conversation>('conversations').doc(conversationId)
      .collection<Partial<Message>>('messages').add({...messageData});
  }

}
