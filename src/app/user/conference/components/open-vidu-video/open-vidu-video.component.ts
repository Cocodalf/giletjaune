import { Component, ElementRef, ViewChild, AfterViewInit, Input } from '@angular/core';
import { StreamManager } from 'openvidu-browser';

@Component({
  selector: 'app-open-vidu-video',
  templateUrl: './open-vidu-video.component.html',
  styleUrls: ['./open-vidu-video.component.scss'],
})
export class OpenViduVideoComponent implements AfterViewInit {

  @ViewChild('videoElement') videoElement: ElementRef;
  @Input() streamManager: StreamManager;

  ngAfterViewInit() {
    this.streamManager.addVideoElement(this.videoElement.nativeElement);
  }

}
