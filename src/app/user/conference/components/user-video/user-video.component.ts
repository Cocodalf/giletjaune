import { Component, Input } from '@angular/core';
import { StreamManager } from 'openvidu-browser';

@Component({
  selector: 'app-user-video',
  templateUrl: './user-video.component.html',
  styleUrls: ['./user-video.component.scss'],
})
export class UserVideoComponent {

  @Input() streamManager: StreamManager;

  getNicknameTag() {
    try {
      return JSON.parse(this.streamManager.stream.connection.data).clientData.displayName;
    } catch (err) {
      console.error('ClientData is not JSON formatted');
    }
  }

}
