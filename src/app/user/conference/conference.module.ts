import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { ConferencePage } from './conference.page';
import { OpenViduService } from '../user-services/open-vidu.service';
import { HttpClientModule } from '@angular/common/http';
import { OpenViduVideoComponent } from './components/open-vidu-video/open-vidu-video.component';
import { UserVideoComponent } from './components/user-video/user-video.component';
import { UserSharedModule } from '../user-shared/shared.module';

const routes: Routes = [
  {
    path: '',
    component: ConferencePage
  }
];

@NgModule({
  imports: [
    UserSharedModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    ConferencePage,
    OpenViduVideoComponent,
    UserVideoComponent
  ],
  providers: [OpenViduService]
})
export class ConferencePageModule {}
