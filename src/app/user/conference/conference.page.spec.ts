import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConferencePage } from './conference.page';

describe('ConferencePage', () => {
  let component: ConferencePage;
  let fixture: ComponentFixture<ConferencePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConferencePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConferencePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
