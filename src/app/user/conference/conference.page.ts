import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { Store, select } from '@ngrx/store';
import { Session, StreamManager } from 'openvidu-browser';
import { Subscription } from 'rxjs';

import { OpenViduService } from '../user-services/open-vidu.service';
import { UserState, UserSelectors } from '../user-store';

@Component({
  selector: 'app-conference',
  templateUrl: './conference.page.html',
  styleUrls: ['./conference.page.scss'],
})
export class ConferencePage implements OnInit {

  conferenceId: string;
  profileSub: Subscription;

  constructor(
    private route: ActivatedRoute,
    private store$: Store<UserState.State>,
    private openViduService: OpenViduService,
  ) { }

  ngOnInit() {
    this.conferenceId = this.route.snapshot.params['conferenceId'];
    this.profileSub = this.store$.pipe(select(UserSelectors.selectProfileState)).subscribe(profile => {
      this.openViduService.join(profile, this.conferenceId);
    });
  }

  ionViewWillLeave() {
    if (this.profileSub) {
      this.profileSub.unsubscribe();
    }
    this.openViduService.leave();
  }

  get session(): Session {
    return this.openViduService.session;
  }

  get publisher(): StreamManager {
    return this.openViduService.publisher;
  }

  get subscribers(): StreamManager[] {
    return this.openViduService.subscribers;
  }

}
