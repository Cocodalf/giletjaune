import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Platform } from '@ionic/angular';
import { FileChooser } from '@ionic-native/file-chooser/ngx';

import { UserState, UserSelectors, ProfileActions } from '../user-store';
import { User } from 'src/app/shared/models/user';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  profile$: Observable<User> = this.store$.pipe(select(UserSelectors.selectProfileState));

  displayName = new FormControl('', [
    Validators.required,
    Validators.minLength(4)
  ]);

  profileForm: FormGroup = new FormGroup({ displayName: this.displayName });

  choosenFile: string | ArrayBuffer;

  isAndroid: boolean;

  constructor(
    private store$: Store<UserState.State>,
    private platform: Platform,
    private fileChooser: FileChooser
  ) { }

  ngOnInit() {
    this.profile$.subscribe(profile => this.displayName.setValue(profile.displayName));
    this.isAndroid = this.platform.is('cordova') && this.platform.is('android');
  }

  handleSubmit() {
    this.store$.dispatch(new ProfileActions.UpdateDisplayNameAction(this.displayName.value));
  }

  handleAvatarChange(event: any) {
    const [file] = event.target.files;
    this.processFile(file);
  }

  openFileChooser() {
    if (!this.isAndroid) { return; }
    this.fileChooser.open()
      .then(uri => this.resolveFile(uri));
  }

  private resolveFile(nativePath: string) {
    (<any>window).resolveLocalFileSystemURL(nativePath, res => {
      res.file(file => this.processFile(file));
    });
  }

  private processFile(file) {
    const reader = new FileReader();
    reader.onloadend = (event: any) => {
      this.choosenFile = event.target.result;
    };
    reader.readAsDataURL(file);
  }

}
