import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { userReducer } from './reducer';

import { ProfileEffects } from './profile-store/effects';
import { FriendEffects } from './friend-store/effects';
import { ContactEffects } from './contact-store/effects';
import { RequestEffects } from './request-store/effects';
import { ConversationEffects } from './conversation-store/effects';
import { MessageEffects } from './message-store/effects';

import { ProfileService } from '../user-services/profile.service';
import { FriendService } from '../user-services/friend.service';
import { ContactService } from '../user-services/contact.service';
import { RequestService } from '../user-services/request.service';
import { ConversationService } from '../user-services/conversation.service';
import { MessageService } from '../user-services/message.service';

@NgModule({
  imports: [
    StoreModule.forFeature('user', userReducer),
    EffectsModule.forFeature([
      ProfileEffects,
      FriendEffects,
      ContactEffects,
      RequestEffects,
      ConversationEffects,
      MessageEffects
    ])
  ],
  providers: [
    ProfileService,
    FriendService,
    ContactService,
    RequestService,
    ConversationService,
    MessageService
  ]
})
export class UserStoreModule { }
