import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, from, of } from 'rxjs';
import { Action, Store, select } from '@ngrx/store';
import { switchMap, map, withLatestFrom, catchError } from 'rxjs/operators';

import * as actions from './actions';
import { FriendService } from '../../user-services/friend.service';
import { RequestActions } from '../request-store';
import { AuthSelectors, CoreState, AuthActions } from 'src/app/core/core-store';
import { ConversationActions } from '../conversation-store';

@Injectable()
export class FriendEffects {

  @Effect()
  load$: Observable<Action> = this.actions$.pipe(
    ofType<actions.LoadAction>(actions.Types.LOAD),
    withLatestFrom(this.store$.pipe(select(AuthSelectors.selectAuthUid))),
    switchMap(([action, authId]) => this.friendService.load(authId)),
    map(({ type, payload }) => {
      return {
        type: `[Friend] ${type}`,
        payload: { ...payload.doc.data(), uid: payload.doc.id }
      };
    })
  );

  @Effect()
  create$: Observable<Action> = this.actions$.pipe(
    ofType<actions.CreateAction>(actions.Types.CREATE),
    withLatestFrom(this.store$.pipe(select(AuthSelectors.selectAuthUid))),
    switchMap(([action, authId]) => from(this.friendService.create(action.payload.requestId, action.payload.friendId, authId)).pipe(
      map((requestId) => new actions.CreateSuccessAction(requestId)),
      catchError((err: Error) => of(new actions.ErrorAction(err.message)))
    )),
  );

  @Effect()
  createSuccess$: Observable<Action> = this.actions$.pipe(
    ofType<actions.CreateSuccessAction>(actions.Types.CREATE_SUCCESS),
    map(action => new RequestActions.DeleteAction(action.payload)),
  );

  @Effect()
  delete$: Observable<Action> = this.actions$.pipe(
    ofType<actions.DeleteAction>(actions.Types.DELETE),
    withLatestFrom(this.store$.pipe(select(AuthSelectors.selectAuthUid))),
    switchMap(([action, authId]) => from(this.friendService.delete(action.payload, authId)).pipe(
      map(usersIds => new actions.DeleteSuccessAction(usersIds)),
      catchError((err: Error) => of(new actions.ErrorAction(err.message)))
    )),
  );

  @Effect()
  deleteSuccess$: Observable<Action> = this.actions$.pipe(
    ofType<actions.DeleteSuccessAction>(actions.Types.DELETE_SUCCESS),
    map(action => new ConversationActions.DeleteAction(action.payload))
  );

  @Effect()
  signOut$: Observable<Action> = this.actions$.pipe(
    ofType<AuthActions.SignOutAction>(AuthActions.Types.SIGN_OUT),
    map(() => new actions.ClearAction())
  );

  constructor(
    private actions$: Actions,
    private store$: Store<CoreState.State>,
    private friendService: FriendService,
  ) { }
}
