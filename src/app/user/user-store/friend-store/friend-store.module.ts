import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { friendReducer } from './reducer';
import { FriendEffects } from './effects';

@NgModule({
  imports: [
    StoreModule.forFeature('friends', friendReducer),
    EffectsModule.forFeature([FriendEffects])
  ]
})
export class FriendStoreModule { }
