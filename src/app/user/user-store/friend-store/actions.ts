import { Action } from '@ngrx/store';
import { User } from 'src/app/shared/models/user';

export enum Types {
  LOAD = '[Friend] load',
  CREATE = '[Friend] create',
  CREATE_SUCCESS = '[Friend] create success',
  DELETE = '[Friend] delete',
  DELETE_SUCCESS = '[Friend] delete success',
  ADDED = '[Friend] added',
  MODIFIED = '[Friend] modified',
  REMOVED = '[Friend] removed',
  CLEAR = '[Friend] clear',
  ERROR = '[Friend] error',
}

export class LoadAction implements Action {
  readonly type = Types.LOAD;
}

export class CreateAction implements Action {
  readonly type = Types.CREATE;
  constructor(public payload: { requestId: string, friendId: string }) { }
}

export class CreateSuccessAction implements Action {
  readonly type = Types.CREATE_SUCCESS;
  constructor(public payload: string) { }
}

export class DeleteAction implements Action {
  readonly type = Types.DELETE;
  constructor(public payload: string) { }
}

export class DeleteSuccessAction implements Action {
  readonly type = Types.DELETE_SUCCESS;
  constructor(public payload: string[]) { }
}

export class AddedAction implements Action {
  readonly type = Types.ADDED;
  constructor(public payload: User) { }
}

export class ModifiedAction implements Action {
  readonly type = Types.MODIFIED;
  constructor(public payload: User) { }
}

export class RemovedAction implements Action {
  readonly type = Types.REMOVED;
  constructor(public payload: User) { }
}

export class ClearAction implements Action {
  readonly type = Types.CLEAR;
}

export class ErrorAction implements Action {
  readonly type = Types.ERROR;
  constructor(public payload: string ) { }
}

export type All =
  LoadAction
  | CreateAction
  | CreateSuccessAction
  | DeleteAction
  | DeleteSuccessAction
  | AddedAction
  | ModifiedAction
  | RemovedAction
  | ClearAction
  | ErrorAction;
