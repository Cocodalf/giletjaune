import { Action } from '@ngrx/store';
import { User } from 'src/app/shared/models/user';

export enum Types {
  LOAD = '[Contact] load',
  ADDED = '[Contact] added',
  MODIFIED = '[Contact] modified',
  REMOVED = '[Contact] removed',
  CLEAR = '[Contact] clear',
  ERROR = '[Contact] error',
}

export class LoadAction implements Action {
  readonly type = Types.LOAD;
}

export class AddedAction implements Action {
  readonly type = Types.ADDED;
  constructor(public payload: User) { }
}

export class ModifiedAction implements Action {
  readonly type = Types.MODIFIED;
  constructor(public payload: User) { }
}

export class RemovedAction implements Action {
  readonly type = Types.REMOVED;
  constructor(public payload: User) { }
}

export class ClearAction implements Action {
  readonly type = Types.CLEAR;
}

export class ErrorAction implements Action {
  readonly type = Types.ERROR;
  constructor(public payload: string ) { }
}

export type All =
  LoadAction
  | AddedAction
  | ModifiedAction
  | RemovedAction
  | ClearAction
  | ErrorAction;
