import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { Action, Store, select } from '@ngrx/store';
import { switchMap, map, withLatestFrom, startWith } from 'rxjs/operators';

import * as actions from './actions';
import { ContactService } from '../../user-services/contact.service';
import { AuthState, AuthSelectors, AuthActions } from 'src/app/core/core-store';

@Injectable()
export class ContactEffects {

  @Effect()
  load$: Observable<Action> = this.actions$.pipe(
    ofType<actions.LoadAction>(actions.Types.LOAD),
    withLatestFrom(this.store$.pipe(select(AuthSelectors.selectAuthUid))),
    switchMap(([action, authId]) => this.contactService.load(authId)),
    map(({ type, payload }) => {
      return {
        type: `[Contact] ${type}`,
        payload: { ...payload.doc.data(), uid: payload.doc.id }
      };
    })
  );

  @Effect()
  signOut$: Observable<Action> = this.actions$.pipe(
    ofType<AuthActions.SignOutAction>(AuthActions.Types.SIGN_OUT),
    map(() => new actions.ClearAction())
  );

  constructor(
    private actions$: Actions,
    private store$: Store<AuthState.State>,
    private contactService: ContactService
  ) { }
}
