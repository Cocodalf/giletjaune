import { createEntityAdapter, EntityState } from '@ngrx/entity';

import { User } from 'src/app/shared/models/user';

export const adapter = createEntityAdapter<User>({
  selectId: item => item.uid
});

export interface State extends EntityState<User> {}

export const initialState: State = adapter.getInitialState();
