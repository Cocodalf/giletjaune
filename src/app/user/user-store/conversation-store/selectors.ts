import { adapter, State } from './state';

export const getSelected = (state: State) => state.selected;
export const getIsLoading = (state: State) => state.isLoading;
export const { selectAll } = adapter.getSelectors();

