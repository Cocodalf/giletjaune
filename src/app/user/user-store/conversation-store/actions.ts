import { Action } from '@ngrx/store';

import { Conversation } from '../../user-shared/models/conversation';

export enum Types {
  LOAD = '[Conversation] load',
  CREATE_PRIVATE = '[Conversation] create private',
  CREATE_PUBLIC = '[Conversation] create public',
  CREATE_SUCCESS = '[Conversation] create success',
  UPDATE = '[Conversation] update',
  UPDATE_SUCCESS = '[Conversation] update success',
  DELETE = '[Conversation] delete',
  DELETE_SUCCESS = '[Conversation] delete success',
  SELECT = '[Conversation] select',
  ADDED = '[Conversation] added',
  MODIFIED = '[Conversation] modified',
  REMOVED = '[Conversation] removed',
  CLEAR = '[Conversation] clear',
  ERROR = '[Conversation] error',
}

export class LoadAction implements Action {
  readonly type = Types.LOAD;
}

export class CreatePrivateAction implements Action {
  readonly type = Types.CREATE_PRIVATE;
  constructor(public payload: string) { }
}

export class CreatePublicAction implements Action {
  readonly type = Types.CREATE_PUBLIC;
  constructor(public payload: { displayName: string, friendIds: string[] }) { }
}

export class CreateSuccessAction implements Action {
  readonly type = Types.CREATE_SUCCESS;
  constructor(public payload: string) { }
}

export class UpdateAction implements Action {
  readonly type = Types.UPDATE;
  constructor(public payload: any) { }
}

export class UpdateSuccessAction implements Action {
  readonly type = Types.UPDATE_SUCCESS;
}

export class DeleteAction implements Action {
  readonly type = Types.DELETE;
  constructor(public payload: string[]) { }
}

export class DeleteSuccessAction implements Action {
  readonly type = Types.DELETE_SUCCESS;
}

export class SelectAction implements Action {
  readonly type = Types.SELECT;
  constructor(public payload: string) { }
}

export class AddedAction implements Action {
  readonly type = Types.ADDED;
  constructor(public payload: Conversation) { }
}

export class ModifiedAction implements Action {
  readonly type = Types.MODIFIED;
  constructor(public payload: Conversation) { }
}

export class RemovedAction implements Action {
  readonly type = Types.REMOVED;
  constructor(public payload: Conversation) { }
}

export class ClearAction implements Action {
  readonly type = Types.CLEAR;
}

export class ErrorAction implements Action {
  readonly type = Types.ERROR;
  constructor(public payload: string ) { }
}

export type All =
  LoadAction
  | CreatePrivateAction
  | CreatePublicAction
  | CreateSuccessAction
  | UpdateAction
  | UpdateSuccessAction
  | DeleteAction
  | DeleteSuccessAction
  | SelectAction
  | AddedAction
  | ModifiedAction
  | RemovedAction
  | ClearAction
  | ErrorAction;
