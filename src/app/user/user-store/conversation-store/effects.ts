import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, from, of } from 'rxjs';
import { Action, Store, select } from '@ngrx/store';
import { switchMap, map, withLatestFrom, catchError, mergeMap, startWith } from 'rxjs/operators';

import * as actions from './actions';
import { MessageActions } from '../message-store';
import { ConversationService } from '../../user-services/conversation.service';
import { CoreState, AuthSelectors, AuthActions, RouterActions } from 'src/app/core/core-store';

@Injectable()
export class ConversationEffects {

  @Effect()
  load$: Observable<Action> = this.actions$.pipe(
    ofType<actions.LoadAction>(actions.Types.LOAD),
    withLatestFrom(this.store$.pipe(select(AuthSelectors.selectAuthUid))),
    switchMap(([action, authId]) => this.conversationService.load(authId)),
    map(({ action, members }) => {
      return {
        type: `[Conversation] ${action.type}`,
        payload: {
          ...action.payload.doc.data(),
          uid: action.payload.doc.id,
          members
        }
      };
    })
  );

  @Effect()
  createPrivate$: Observable<Action> = this.actions$.pipe(
    ofType<actions.CreatePrivateAction>(actions.Types.CREATE_PRIVATE),
    withLatestFrom(this.store$.pipe(select(AuthSelectors.selectAuthUid))),
    switchMap(([action, authId]) => from(this.conversationService.createPrivate(action.payload, authId)).pipe(
      map((conversationId) => new actions.CreateSuccessAction(conversationId)),
      catchError((err: Error) => of(new actions.ErrorAction(err.message)))
    )),
  );

  @Effect()
  createPublic$: Observable<Action> = this.actions$.pipe(
    ofType<actions.CreatePublicAction>(actions.Types.CREATE_PUBLIC),
    withLatestFrom(this.store$.pipe(select(AuthSelectors.selectAuthUid))),
    switchMap(([action, authId]) => from(this.conversationService.createPublic(action.payload, authId)).pipe(
      map((conversationId) => new actions.CreateSuccessAction(conversationId)),
      catchError((err: Error) => of(new actions.ErrorAction(err.message)))
    )),
  );

  @Effect()
  createSuccess$: Observable<Action> = this.actions$.pipe(
    ofType<actions.CreateSuccessAction>(actions.Types.CREATE_SUCCESS),
    mergeMap(action => [
      new RouterActions.GoAction({ path: ['user', 'conversation', action.payload] }),
      new actions.SelectAction(action.payload)
    ])
  );

  @Effect()
  update$: Observable<Action> = this.actions$.pipe(
    ofType<actions.UpdateAction>(actions.Types.UPDATE),
    switchMap(action => from(this.conversationService.update(action.payload)).pipe(
      map(() => new actions.UpdateSuccessAction()),
      catchError((err: Error) => of(new actions.ErrorAction(err.message)))
    ))
  );

  @Effect()
  select$: Observable<Action> = this.actions$.pipe(
    ofType<actions.SelectAction>(actions.Types.SELECT),
    mergeMap(action => [
      new MessageActions.ClearAction(),
      new MessageActions.LoadAction(action.payload),
    ])
  );

  @Effect()
  delete$: Observable<Action> = this.actions$.pipe(
    ofType<actions.DeleteAction>(actions.Types.DELETE),
    switchMap(action => from(this.conversationService.getByUsers(action.payload)).pipe(
      map(conversation => this.conversationService.delete(conversation.uid)),
      map(() => new actions.DeleteSuccessAction()),
      catchError((err: Error) => of(new actions.ErrorAction(err.message)))
    ))
  );

  @Effect()
  signOut$: Observable<Action> = this.actions$.pipe(
    ofType<AuthActions.SignOutAction>(AuthActions.Types.SIGN_OUT),
    map(() => new actions.ClearAction())
  );

  @Effect()
  clear$: Observable<Action> = this.actions$.pipe(
    ofType<actions.ClearAction>(actions.Types.CLEAR),
    map(() => new MessageActions.ClearAction())
  );

  constructor(
    private actions$: Actions,
    private store$: Store<CoreState.State>,
    private conversationService: ConversationService,
  ) { }
}
