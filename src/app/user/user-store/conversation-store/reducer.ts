import { State, initialState, adapter } from './state';
import * as actions from './actions';

export function conversationReducer(state: State = initialState, action: actions.All) {
  switch (action.type) {
    case actions.Types.CREATE_PRIVATE:
      return { ...state, isLoading: true };
    case actions.Types.CREATE_PUBLIC:
      return { ...state, isLoading: true };
    case actions.Types.CREATE_SUCCESS:
      return { ...state, isLoading: false };
    case actions.Types.UPDATE:
      return { ...state, isLoading: true };
    case actions.Types.UPDATE_SUCCESS:
      return { ...state, isLoading: false };
    case actions.Types.DELETE:
      return { ...state, isLoading: true };
    case actions.Types.DELETE_SUCCESS:
      return { ...state, isLoading: false };
    case actions.Types.SELECT:
      return { ...state, selected: action.payload };
    case actions.Types.ADDED:
      return adapter.addOne(action.payload, state);
    case actions.Types.MODIFIED:
      return adapter.updateOne({ changes: action.payload, id: action.payload.uid }, state);
    case actions.Types.REMOVED:
      return adapter.removeOne(action.payload.uid, state);
    case actions.Types.CLEAR:
      return initialState;
    case actions.Types.ERROR:
      return { ...state, isLoading: false, error: action.payload };
    default:
      return state;
  }
}
