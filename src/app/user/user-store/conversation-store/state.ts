import { createEntityAdapter, EntityState } from '@ngrx/entity';

import { Conversation } from '../../user-shared/models/conversation';

export const adapter = createEntityAdapter<Conversation>({
  selectId: item => item.uid
});

export interface State extends EntityState<Conversation> {
  selected: string;
  isLoading: boolean;
}

export const initialState: State = adapter.getInitialState({
  selected: null,
  isLoading: false,
});
