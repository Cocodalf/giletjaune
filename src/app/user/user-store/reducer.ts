import { profileReducer } from './profile-store/reducer';
import { friendReducer } from './friend-store/reducer';
import { requestReducer } from './request-store/reducer';
import { conversationReducer } from './conversation-store/reducer';
import { messageReducer } from './message-store/reducer';
import { contactReducer } from './contact-store/reducer';

export const userReducer = {
  profile: profileReducer,
  friend: friendReducer,
  request: requestReducer,
  conversation: conversationReducer,
  message: messageReducer,
  contact: contactReducer,
};

