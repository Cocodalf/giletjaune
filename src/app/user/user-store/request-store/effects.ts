import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, from, of } from 'rxjs';
import { Action, Store, select } from '@ngrx/store';
import { switchMap, map, withLatestFrom, catchError } from 'rxjs/operators';

import * as actions from './actions';

import { CoreState, AuthSelectors, AuthActions } from 'src/app/core/core-store';
import { RequestService } from '../../user-services/request.service';

@Injectable()
export class RequestEffects {

  @Effect()
  load$: Observable<Action> = this.actions$.pipe(
    ofType<actions.LoadAction>(actions.Types.LOAD),
    withLatestFrom(this.store$.pipe(select(AuthSelectors.selectAuthUid))),
    switchMap(([action, authId]) => this.requestService.load(authId)),
    map(({ action, target }) => {
        return {
          type: `[Request] ${action.type}`,
          payload: {
            uid: action.payload.doc.id,
            target,
            ...action.payload.doc.data(),
          }
        };
      }
    )
  );

  @Effect()
  create$: Observable<Action> = this.actions$.pipe(
    ofType<actions.CreateAction>(actions.Types.CREATE),
    withLatestFrom(this.store$.pipe(select(AuthSelectors.selectAuthUid))),
    switchMap(([action, authId]) => from(this.requestService.create(authId, action.payload)).pipe(
      map(() => new actions.CreateSuccessAction()),
      catchError((err: Error) => of(new actions.ErrorAction(err.message)))
    )),
  );

  @Effect()
  delete$: Observable<Action> = this.actions$.pipe(
    ofType<actions.CreateAction>(actions.Types.DELETE),
    switchMap(action => from(this.requestService.delete(action.payload)).pipe(
      map(() => new actions.DeleteSuccessAction()),
      catchError((err: Error) => of(new actions.ErrorAction(err.message)))
    )),
  );

  @Effect()
  signOut$: Observable<Action> = this.actions$.pipe(
    ofType<AuthActions.SignOutAction>(AuthActions.Types.SIGN_OUT),
    map(() => new actions.ClearAction())
  );

  constructor(
    private actions$: Actions,
    private store$: Store<CoreState.State>,
    private requestService: RequestService,
  ) { }
}
