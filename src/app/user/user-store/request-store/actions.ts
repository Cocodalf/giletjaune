import { Action } from '@ngrx/store';

import { Request } from '../../user-shared/models/request';

export enum Types {
  LOAD = '[Request] load',
  CREATE = '[Request] create',
  CREATE_SUCCESS = '[Request] create success',
  DELETE = '[Request] delete',
  DELETE_SUCCESS = '[Request] delete success',
  ADDED = '[Request] added',
  MODIFIED = '[Request] modified',
  REMOVED = '[Request] removed',
  CLEAR = '[Request] clear',
  ERROR = '[Request] error',
}

export class LoadAction implements Action {
  readonly type = Types.LOAD;
}

export class CreateAction implements Action {
  readonly type = Types.CREATE;
  constructor(public payload: string) { }
}

export class CreateSuccessAction implements Action {
  readonly type = Types.CREATE_SUCCESS;
}

export class DeleteAction implements Action {
  readonly type = Types.DELETE;
  constructor(public payload: string) { }
}

export class DeleteSuccessAction implements Action {
  readonly type = Types.DELETE_SUCCESS;
}

export class AddedAction implements Action {
  readonly type = Types.ADDED;
  constructor(public payload: Request) { }
}

export class ModifiedAction implements Action {
  readonly type = Types.MODIFIED;
  constructor(public payload: Request) { }
}

export class RemovedAction implements Action {
  readonly type = Types.REMOVED;
  constructor(public payload: Request) { }
}

export class ClearAction implements Action {
  readonly type = Types.CLEAR;
}

export class ErrorAction implements Action {
  readonly type = Types.ERROR;
  constructor(public payload: string) { }
}

export type All =
  LoadAction
  | CreateAction
  | CreateSuccessAction
  | DeleteAction
  | DeleteSuccessAction
  | AddedAction
  | ModifiedAction
  | RemovedAction
  | ClearAction
  | ErrorAction;
