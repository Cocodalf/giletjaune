import { createEntityAdapter, EntityState } from '@ngrx/entity';

import { Request } from '../../user-shared/models/request';

export const adapter = createEntityAdapter<Request>({
  selectId: item => item.uid
});

export interface State extends EntityState<Request> {
  isLoading: boolean;
}

export const initialState: State = adapter.getInitialState({
  isLoading: false
});
