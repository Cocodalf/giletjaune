import { adapter, State } from './state';

export const getIsLoading = (state: State) => state.isLoading;
export const { selectAll } = adapter.getSelectors();

