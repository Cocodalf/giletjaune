import { State, initialState, adapter } from './state';
import * as actions from './actions';

export function requestReducer(state: State = initialState, action: actions.All) {
  switch (action.type) {
    case actions.Types.CREATE:
      return { ...state, isLoading: true };
    case actions.Types.CREATE_SUCCESS:
      return { ...state, isLoading: false };
    case actions.Types.DELETE:
      return { ...state, isLoading: true };
    case actions.Types.DELETE_SUCCESS:
      return { ...state, isLoading: false };
    case actions.Types.ADDED:
      return adapter.addOne(action.payload, state);
    case actions.Types.MODIFIED:
      return adapter.updateOne({ changes: action.payload, id: action.payload.uid }, state);
    case actions.Types.REMOVED:
      return adapter.removeOne(action.payload.uid, state);
    case actions.Types.CLEAR:
      return initialState;
    case actions.Types.ERROR:
      return { ...state, error: action.payload, isLoading: false };
    default:
      return state;
  }
}
