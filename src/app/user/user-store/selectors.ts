import { createFeatureSelector, createSelector } from '@ngrx/store';

import { State } from './state';

import { AuthSelectors } from 'src/app/core/core-store';

import { FriendSelectors } from './friend-store';
import { RequestSelectors } from './request-store';
import { ContactSelectors } from './contact-store';
import { MessageSelectors } from './message-store';
import { ConversationSelectors } from './conversation-store';
import { ProfileSelectors } from './profile-store';

import { User } from 'src/app/shared/models/user';
import { Request } from '../user-shared/models/request';
import { Conversation, ConversationTypes } from '../user-shared/models/conversation';

const getProfile = (state: State) => state.profile;
const getFriend = (state: State) => state.friend;
const getRequest = (state: State) => state.request;
const getContact = (state: State) => state.contact;
const getConversation = (state: State) => state.conversation;
const getMessage = (state: State) => state.message;

export const selectDataState = createFeatureSelector<State>('user');
// profile
export const selectProfileState = createSelector(selectDataState, getProfile);
export const selectProfileIsLoading = createSelector(selectProfileState, ProfileSelectors.getIsLoading);
// friend
export const selectFriendState = createSelector(selectDataState, getFriend);
export const selectAllFriends = createSelector(selectFriendState, FriendSelectors.selectAll);
export const selectFriendIsLoading = createSelector(selectFriendState, FriendSelectors.getIsLoading);
export const selectFriendById = createSelector(
  selectAllFriends,
  (friends: User[], props) => friends.find(friend => friend.uid === props.uid)
);
// request
export const selectRequestState = createSelector(selectDataState, getRequest);
export const selectRequestIsLoading = createSelector(selectRequestState, RequestSelectors.getIsLoading);
export const selectAllRequests = createSelector(selectRequestState, RequestSelectors.selectAll);
export const selectAllRequestsFromAuth = createSelector(
  AuthSelectors.selectAuthUid,
  selectAllRequests,
  (authId: string, requests: Request[]) => requests.filter(request => request.owner === authId)
    .map(request => ({...request, fromAuth: true }))
);
export const selectAllRequestsForAuth = createSelector(
  AuthSelectors.selectAuthUid,
  selectAllRequests,
  (authId: string, requests: Request[]) => requests.filter(request => request.owner !== authId)
    .map(request => ({...request, fromAuth: false }))
);
// contact
export const selectContactState = createSelector(selectDataState, getContact);
const getAllContacts = createSelector(selectContactState, ContactSelectors.selectAll);
export const selectAllContacts = createSelector(
  selectAllFriends,
  selectAllRequests,
  getAllContacts,
  (friends: User[], requests: Request[], contacts: User[]) => contacts
    .filter(contact => !friends.some(friend => friend.uid === contact.uid))
    .filter(contact => !requests.some(request => request.members.some(member => member === contact.uid)))
);
// conversation
export const selectConversationState = createSelector(selectDataState, getConversation);
export const selectConversationIsLoading = createSelector(selectConversationState, ConversationSelectors.getIsLoading);
export const selectAllConversations = createSelector(selectConversationState, ConversationSelectors.selectAll);
export const selectConversationById = createSelector(
  selectAllConversations,
  (conversations: Conversation[], id) => conversations.find(conversation => conversation.uid === id)
);
export const selectSelectedConversation = createSelector(selectConversationState, ConversationSelectors.getSelected);
export const selectAllPrivateConversations = createSelector(
  selectProfileState,
  selectAllConversations,
  (profile: User, conversations: Conversation[]) => conversations
    .filter(conversation => conversation.type === ConversationTypes.PRIVATE)
    .map(conversation => {
    const [friend] = conversation.members.filter(member => member.uid !== profile.uid);
    return {
      ...conversation,
      target: friend
    };
  })
);
export const selectPrivateConversationById = createSelector(
  selectAllPrivateConversations,
  (conversations: Conversation[], id) => conversations.find(conversation => conversation.uid === id)
);
export const selectAllPublicConversations = createSelector(
  selectAllConversations,
  (conversations: Conversation[]) => conversations
    .filter(conversation => conversation.type === ConversationTypes.PUBLIC)
);
export const selectPublicConversationById = createSelector(
  selectAllPublicConversations,
  (conversations: Conversation[], id) => conversations.find(conversation => conversation.uid === id)
);
// message
export const selectMessageState = createSelector(selectDataState, getMessage);
export const selectMessageIsLoading = createSelector(selectMessageState, MessageSelectors.getIsLoading);
export const selectAllMessages = createSelector(selectMessageState, MessageSelectors.selectAll);
// all
export const selectUserIsLoading = createSelector(
  selectProfileIsLoading,
  selectFriendIsLoading,
  selectRequestIsLoading,
  selectConversationIsLoading,
  selectMessageIsLoading,
  (
    profileIsLoading,
    friendIsLoading,
    requestIsLoading,
    conversationIsLoading,
    messageIsLoading
    ) => profileIsLoading
    || friendIsLoading
    || requestIsLoading
    || conversationIsLoading
    || messageIsLoading
);
