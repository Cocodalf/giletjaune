import { Action } from '@ngrx/store';

import { Message } from '../../user-shared/models/message';

export enum Types {
  LOAD = '[Message] load',
  CREATE = '[Message] create',
  CREATE_SUCCESS = '[Message] create success',
  DELETE = '[Message] delete',
  DELETE_SUCCESS = '[Message] delete success',
  ADDED = '[Message] added',
  MODIFIED = '[Message] modified',
  REMOVED = '[Message] removed',
  CLEAR = '[Message] clear',
  ERROR = '[Message] error',
}

export class LoadAction implements Action {
  readonly type = Types.LOAD;
  constructor(public payload: string) { }
}

export class CreateAction implements Action {
  readonly type = Types.CREATE;
  constructor(public payload: { conversationId: string, content: string }) { }
}

export class CreateSuccessAction implements Action {
  readonly type = Types.CREATE_SUCCESS;
}

export class DeleteAction implements Action {
  readonly type = Types.DELETE;
  constructor(public payload: string) { }
}

export class DeleteSuccessAction implements Action {
  readonly type = Types.DELETE_SUCCESS;
}

export class AddedAction implements Action {
  readonly type = Types.ADDED;
  constructor(public payload: Message) { }
}

export class ModifiedAction implements Action {
  readonly type = Types.MODIFIED;
  constructor(public payload: Message) { }
}

export class RemovedAction implements Action {
  readonly type = Types.REMOVED;
  constructor(public payload: Message) { }
}

export class ClearAction implements Action {
  readonly type = Types.CLEAR;
}

export class ErrorAction implements Action {
  readonly type = Types.ERROR;
  constructor(public payload: string) { }
}

export type All =
  LoadAction
  | CreateAction
  | CreateSuccessAction
  | DeleteAction
  | DeleteSuccessAction
  | AddedAction
  | ModifiedAction
  | RemovedAction
  | ClearAction
  | ErrorAction;
