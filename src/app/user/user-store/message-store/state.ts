import { createEntityAdapter, EntityState } from '@ngrx/entity';

import { Message } from '../../user-shared/models/message';

export const adapter = createEntityAdapter<Message>({
  selectId: item => item.uid
});

export interface State extends EntityState<Message> {
  isLoading: boolean;
}

export const initialState: State = adapter.getInitialState({
  isLoading: false
});
