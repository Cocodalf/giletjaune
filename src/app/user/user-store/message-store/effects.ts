import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of, from } from 'rxjs';
import { Action, Store, select } from '@ngrx/store';
import { switchMap, map, withLatestFrom, catchError } from 'rxjs/operators';

import * as actions from './actions';
import { MessageService } from '../../user-services/message.service';
import { CoreState, AuthSelectors } from 'src/app/core/core-store';

@Injectable()
export class MessageEffects {

  @Effect()
  load$: Observable<Action> = this.actions$.pipe(
    ofType<actions.LoadAction>(actions.Types.LOAD),
    switchMap(action => this.messageService.load(action.payload)),
    map(({ type, payload }) => ({ type: `[Message] ${type}`, payload: { ...payload.doc.data(), uid: payload.doc.id }}))
  );

  @Effect()
  create$: Observable<Action> = this.actions$.pipe(
    ofType<actions.CreateAction>(actions.Types.CREATE),
    withLatestFrom(this.store$.pipe(select(AuthSelectors.selectAuthUid))),
    switchMap(([action, authId]) => from(this.messageService.create(authId, action.payload.conversationId, action.payload.content)).pipe(
      map(() => new actions.CreateSuccessAction()),
      catchError((err: Error) => of(new actions.ErrorAction(err.message)))
    )),
  );

  constructor(
    private actions$: Actions,
    private store$: Store<CoreState.State>,
    private messageService: MessageService,
  ) { }
}
