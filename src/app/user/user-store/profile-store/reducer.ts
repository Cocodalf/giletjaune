import { State, initialState } from './state';
import * as actions from './actions';

export function profileReducer(state: State = initialState, action: actions.All) {
  switch (action.type) {
    case actions.Types.LOAD:
      return { ...state, isLoaded: false, isLoading: true };
    case actions.Types.LOAD_SUCCESS:
      return { ...state, ...action.payload, isLoaded: true, isLoading: false };
    case actions.Types.UPDATE_DISPLAYNAME:
      return { ...state, isLoading: true, dispalyName: action.payload };
    case actions.Types.UPDATE_STATUS:
      return { ...state, isLoading: true, status: action.payload };
    case actions.Types.UPDATE_SUCCESS:
      return { ...state, isLoading: false };
    case actions.Types.CLEAR:
      return initialState;
    case actions.Types.ERROR:
      return { ...state, error: action.payload, isLoading: false };
    default:
      return state;
  }
}
