import { State } from './state';

export const getIsLoaded =  (state: State) => state.isLoaded;
export const getIsLoading = (state: State) => state.isLoading;
export const getError = (state: State) => state.error;

