import { User } from 'src/app/shared/models/user';

export interface State extends User {
  isLoaded: boolean;
  isLoading: boolean;
  error?: string;
}

export const initialState: State = {
  uid: null,
  displayName: null,
  email: null,
  phoneNumber: null,
  photoURL: null,
  isLoaded: false,
  isLoading: false,
  friends: []
};
