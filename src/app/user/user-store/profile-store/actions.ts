import { Action } from '@ngrx/store';

import { User } from 'src/app/shared/models/user';

export enum Types {
  LOAD = '[Profile] load',
  LOAD_SUCCESS = '[Profile] load success',
  UPDATE_DISPLAYNAME = '[Profile] update displayname',
  UPDATE_STATUS = '[Profile] update status',
  UPDATE_SUCCESS = '[Profile] update success',
  CLEAR = '[Profile] clear',
  ERROR = '[Profile] error',
}

export class LoadAction implements Action {
  readonly type = Types.LOAD;
}

export class LoadSuccessAction implements Action {
  readonly type = Types.LOAD_SUCCESS;
  constructor(public payload: User) { }
}

export class UpdateDisplayNameAction implements Action {
  readonly type = Types.UPDATE_DISPLAYNAME;
  constructor(public payload: string) { }
}

export class UpdateStatusAction implements Action {
  readonly type = Types.UPDATE_STATUS;
  constructor(public payload: string) { }
}

export class UpdateSuccessAction implements Action {
  readonly type = Types.UPDATE_SUCCESS;
}
export class ClearAction implements Action {
  readonly type = Types.CLEAR;
}

export class ErrorAction implements Action {
  readonly type = Types.ERROR;
  constructor(public payload: string ) { }
}

export type All =
  LoadAction
  | LoadSuccessAction
  | UpdateDisplayNameAction
  | UpdateStatusAction
  | UpdateSuccessAction
  | ClearAction
  | ErrorAction;
