import { Injectable } from '@angular/core';

import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action, Store, select } from '@ngrx/store';

import { Observable, from, of } from 'rxjs';
import { switchMap, map, withLatestFrom, catchError, delay, mergeMap } from 'rxjs/operators';

import * as actions from './actions';
import { AuthState, AuthSelectors, AuthActions } from 'src/app/core/core-store';
import { ProfileService } from '../../user-services/profile.service';

@Injectable()
export class ProfileEffects {

  @Effect()
  load$: Observable<Action> = this.actions$.pipe(
    ofType<actions.LoadAction>(actions.Types.LOAD),
    withLatestFrom(this.store$.pipe(select(AuthSelectors.selectAuthUid))),
    switchMap(([action, authId]) => this.profileService.load(authId)),
    map((action) => new actions.LoadSuccessAction({ ...action.payload.data(), uid: action.payload.id }))
  );

  @Effect()
  updateDisplayname$: Observable<Action> = this.actions$.pipe(
    ofType<actions.UpdateDisplayNameAction>(actions.Types.UPDATE_DISPLAYNAME),
    withLatestFrom(this.store$.pipe(select(AuthSelectors.selectAuthUid))),
    delay(1000),
    switchMap(([action, authId]) => from(this.profileService.updateDisplayName(authId, action.payload)).pipe(
      map(() => new actions.UpdateSuccessAction()),
      catchError((err: Error) => of(new actions.ErrorAction(err.message)))
    ))
  );

  @Effect()
  updateStatus$: Observable<Action> = this.actions$.pipe(
    ofType<actions.UpdateStatusAction>(actions.Types.UPDATE_STATUS),
    withLatestFrom(this.store$.pipe(select(AuthSelectors.selectAuthUid))),
    delay(1000),
    switchMap(([action, authId]) => from(this.profileService.updateStatus(authId, action.payload)).pipe(
      map(() => new actions.UpdateSuccessAction()),
      catchError((err: Error) => of(new actions.ErrorAction(err.message)))
    ))
  );

  @Effect()
  signOut$: Observable<Action> = this.actions$.pipe(
    ofType<AuthActions.SignOutAction>(AuthActions.Types.SIGN_OUT),
    mergeMap(() => [
      new actions.ClearAction(),
      new actions.UpdateStatusAction('offline')
    ])
  );

  @Effect()
  authenticated$: Observable<Action> = this.actions$.pipe(
    ofType<AuthActions.AuthenticatedAction>(AuthActions.Types.AUTHENTICATED),
    map(() => new actions.UpdateStatusAction('online'))
  );

  constructor(
    private actions$: Actions,
    private store$: Store<AuthState.State>,
    private profileService: ProfileService,
  ) { }
}
