import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { profileReducer } from './reducer';
import { ProfileEffects } from './effects';

@NgModule({
  imports: [
    StoreModule.forFeature('profile', profileReducer),
    EffectsModule.forFeature([ProfileEffects])
  ]
})
export class ProfileStoreModule { }
