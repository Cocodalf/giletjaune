import { ProfileState } from './profile-store';
import { FriendState } from './friend-store';
import { RequestState } from './request-store';
import { ConversationState } from './conversation-store';
import { MessageState } from './message-store';
import { ContactState } from './contact-store';

export interface State {
  profile: ProfileState.State;
  friend: FriendState.State;
  request: RequestState.State;
  conversation: ConversationState.State;
  message: MessageState.State;
  contact: ContactState.State;
}

