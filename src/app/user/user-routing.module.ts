import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserPage } from './user.page';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'profile',
    pathMatch: 'full',
  },
  {
    path: '',
    component: UserPage,
    children: [
      { path: 'profile', loadChildren: './profile/profile.module#ProfilePageModule' },
      {
        path: 'dashboard',
        loadChildren: './dashboard/dashboard.module#DashboardPageModule',
      },
      {
        path: 'conversation/:conversationId',
        loadChildren: './conversation/conversation.module#ConversationPageModule',
      },
      {
        path: 'conference/:conferenceId',
        loadChildren: './conference/conference.module#ConferencePageModule',
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
