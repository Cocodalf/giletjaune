import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { Firebase } from '@ionic-native/firebase/ngx';

import { GuestRoutingModule } from './guest-routing.module';
import { GuestPage } from './guest.page';
import { GuestStoreModule } from './guest-store/guest-store.module';
import { ToastService } from '../core/core-services/toast.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    GuestRoutingModule,
    GuestStoreModule
  ],
  declarations: [GuestPage],
  providers: [
    Firebase,
    ToastService
  ]
})
export class GuestPageModule {}
