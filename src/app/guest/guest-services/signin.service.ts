import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { Firebase } from '@ionic-native/firebase/ngx';
import * as firebase from 'firebase/app';

@Injectable()
export class SigninService {

  constructor(private afAuth: AngularFireAuth, private afs: AngularFirestore, private firebaseIonic: Firebase) { }

  emailSignin(email: string, password: string): Promise<firebase.auth.UserCredential> {
    return this.afAuth.auth.signInWithEmailAndPassword(email, password);
  }

  emailRegister(email: string, password: string): Promise<void> {
    return this.afAuth.auth.createUserWithEmailAndPassword(email, password)
      .then(credential => {
        this.afs.collection('users').doc(credential.user.uid).set({
          email: credential.user.email,
          friends: []
        });
      });
  }

  async phoneSignin(phoneNumber: string) {
    console.log(phoneNumber);
    const credential = await this.firebaseIonic.verifyPhoneNumber(phoneNumber);
    console.log(credential);
    return credential;
  }

}
