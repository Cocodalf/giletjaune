import { createFeatureSelector, createSelector } from '@ngrx/store';
import { State } from './state';
import { SigninState } from './signin';


export const selectGuestState = createFeatureSelector('guest');

export const selectSigninState = createSelector(selectGuestState, (state: State) => state.signin);
export const selectSigninIsLoading = createSelector(selectSigninState, (state: SigninState.State) => state.isLoading);
export const selectError = createSelector(selectSigninState, (state: SigninState.State) => state.error);
