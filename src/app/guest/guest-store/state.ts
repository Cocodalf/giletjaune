import { SigninState } from './signin';

export interface State {
  signin: SigninState.State;
}
