import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { guestReducer } from './reducer';
import { SigninEffects } from './signin/effects';
import { SigninService } from '../guest-services/signin.service';

@NgModule({
  imports: [
    StoreModule.forFeature('guest', guestReducer),
    EffectsModule.forFeature([SigninEffects])
  ],
  providers: [SigninService]
})
export class GuestStoreModule { }
