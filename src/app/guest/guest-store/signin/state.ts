export interface State {
  isLoading: boolean;
  credential: null;
  error?: string;
}

export const initialState: State = {
  isLoading: false,
  credential: null
};
