import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';

import * as actions from './actions';
import { Observable, from, of } from 'rxjs';
import { switchMap, catchError, map, tap } from 'rxjs/operators';
import { SigninService } from '../../guest-services/signin.service';
import { AuthActions } from 'src/app/core/core-store';
import { ToastService } from 'src/app/core/core-services/toast.service';

@Injectable()
export class SigninEffects {

  @Effect()
  signin$: Observable<Action> = this.actions$.pipe(
    ofType<actions.SigninRequestAction>(actions.Types.SIGNIN_REQUEST),
    switchMap(action => from(this.signinService.phoneSignin(action.payload.phoneNumber)).pipe(
      map((credential) => new actions.SigninSuccessAction(credential)),
      catchError((err: Error) => of(new actions.SigninErrorAction(err.message)))
    ))
  );

  @Effect()
  signinSuccess$: Observable<Action> = this.actions$.pipe(
    ofType<actions.SigninSuccessAction>(actions.Types.SIGNIN_SUCCESS),
    tap((action) => this.toastService.showGreetings('user')),
    map(action => new AuthActions.AuthenticatedAction(action.payload.user.uid))
  );

  @Effect()
  signOut$: Observable<Action> = this.actions$.pipe(
    ofType<AuthActions.SignOutAction>(AuthActions.Types.SIGN_OUT),
    map(() => new actions.ClearAction())
  );

  constructor(private actions$: Actions, private signinService: SigninService, private toastService: ToastService) { }
}
