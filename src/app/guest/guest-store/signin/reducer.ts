import * as actions from './actions';
import { State, initialState } from './state';

export function signinReducer(state: State = initialState, action: actions.All) {
  switch (action.type) {
    case actions.Types.SIGNIN_REQUEST:
      return { ...state, isLoading: true };
    case actions.Types.SIGNIN_SUCCESS:
      return { ...state, isLoading: false, credential: action.payload };
    case actions.Types.SIGNIN_ERROR:
      return { ...state, isLoading: false, error: action.payload };
    case actions.Types.CLEAR:
      return initialState;
    default:
      return state;
  }
}
