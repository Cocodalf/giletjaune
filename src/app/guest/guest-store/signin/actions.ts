import { Action } from '@ngrx/store';
import * as firebase from 'firebase/app';

export enum Types {
  SIGNIN_REQUEST = '[Signin] signin request',
  SIGNIN_SUCCESS = '[Signin] signin success',
  SIGNIN_ERROR = '[Signin] signin error',
  CLEAR = '[Signin] clear',
}

export class SigninRequestAction implements Action {
  readonly type = Types.SIGNIN_REQUEST;
  constructor(public payload: { phoneNumber: string }) { }
}

export class SigninSuccessAction implements Action {
  readonly type = Types.SIGNIN_SUCCESS;
  constructor(public payload: firebase.auth.UserCredential) { }
}

export class SigninErrorAction implements Action {
  readonly type = Types.SIGNIN_ERROR;
  constructor(public payload: string) { }
}

export class ClearAction implements Action {
  readonly type = Types.CLEAR;
}

export type All =
  SigninRequestAction
  | SigninErrorAction
  | SigninSuccessAction
  | ClearAction;
