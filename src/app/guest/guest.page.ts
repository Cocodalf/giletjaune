import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormGroup, FormControl, Validators, ValidatorFn, AbstractControl } from '@angular/forms';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Platform, AlertController } from '@ionic/angular';

import { GuestState, GuestSelectors, SigninActions } from './guest-store';
import { Firebase } from '@ionic-native/firebase/ngx';
import { auth } from 'firebase/app';
import { AuthActions } from '../core/core-store';
import { AngularFireAuth } from '@angular/fire/auth';

export function frenchPhoneValidator() {
  const validator: ValidatorFn = (phoneNumber: AbstractControl) => {
    const frenchPhoneRegex = /^((\+)33|0|0033)[1-9](\d{2}){4}$/g;
    return frenchPhoneRegex.test(phoneNumber.value) ? null : { pattern: false };
  };
  return validator;
}

/*
TODO: refacto
*/
@Component({
  selector: 'app-guest',
  templateUrl: './guest.page.html',
  styleUrls: ['./guest.page.scss'],
})
export class GuestPage implements OnInit, AfterViewInit {

  isLoading$: Observable<boolean> = this.store$.pipe(select(GuestSelectors.selectSigninIsLoading));
  signinError$: Observable<string> = this.store$.pipe(select(GuestSelectors.selectError));

  isAndroid: boolean;
  recaptchaVerifier;

  authForm: FormGroup = new FormGroup({
    phoneNumber: new FormControl('', [
      Validators.required,
      Validators.pattern(/^((\+)33|0|0033)[1-9](\d{2}){4}$/g),
    ])
  });

  get phoneNumber() {
    return this.authForm.get('phoneNumber');
  }

  constructor(
    private store$: Store<GuestState.State>,
    private platform: Platform,
    private fireAuth: AngularFireAuth,
    private fireNative: Firebase,
    private alertCtrl: AlertController
  ) { }

  ngOnInit() {
    this.isAndroid = this.platform.is('cordova') && this.platform.is('android');
  }

  ngAfterViewInit() {
    this.recaptchaVerifier = new auth.RecaptchaVerifier('recaptcha-container');
    this.recaptchaVerifier.render();
  }

  async signIn() {
    let credential;
    try {
      if (this.isAndroid) {
        credential = await this.fireNative.verifyPhoneNumber(this.authForm.controls.phoneNumber.value, 120);
      } else {
        credential = await this.fireAuth.auth.signInWithPhoneNumber(
          this.authForm.controls.phoneNumber.value,
          this.recaptchaVerifier
        );
      }
    } catch (err) {
      this.store$.dispatch(new SigninActions.SigninErrorAction(err.message));
    }
    if (credential) {
      this.showVerificationAlert(credential.verificationId);
    }
  }

  private async showVerificationAlert(verificationId: string) {
    const alert = await this.alertCtrl.create({
      header: 'verify',
      message: 'Type code that was received via SMS',
      inputs: [
        {
          name: 'code',
          type: 'tel',
          placeholder: 'Code'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Verify',
          handler: data => {
            this.verifyCode(data.code, verificationId);
          }
        },
      ]
    });
    await alert.present();
  }

  private async verifyCode(code: string, verificationId: string): Promise<void> {
    try {
      const signInCredential = auth.PhoneAuthProvider.credential(verificationId, code);
      const credential = await auth().signInAndRetrieveDataWithCredential(signInCredential);
      this.store$.dispatch(new SigninActions.SigninSuccessAction(credential));
    } catch (err) {
      this.store$.dispatch(new SigninActions.SigninErrorAction(err.message));
    }
  }
}
