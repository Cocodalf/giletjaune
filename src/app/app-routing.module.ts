import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from './core/core-services/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'jitsi', pathMatch: 'full' },
  {
    path: 'guest',
    loadChildren: './guest/guest.module#GuestPageModule'
  },
  {
    path: 'user',
    loadChildren: './user/user.module#UserPageModule',
    canLoad: [AuthGuard]
  },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class AppRoutingModule { }
