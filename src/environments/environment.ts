// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyC_SvqyaNmEFt987j9tU8YA9l8pH46l6XY',
    authDomain: 'yellow-jackets-2d348.firebaseapp.com',
    databaseURL: 'https://yellow-jackets-2d348.firebaseio.com',
    projectId: 'yellow-jackets-2d348',
    storageBucket: 'yellow-jackets-2d348.appspot.com',
    messagingSenderId: '169299527581'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
