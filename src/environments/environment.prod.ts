export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyC_SvqyaNmEFt987j9tU8YA9l8pH46l6XY',
    authDomain: 'yellow-jackets-2d348.firebaseapp.com',
    databaseURL: 'https://yellow-jackets-2d348.firebaseio.com',
    projectId: 'yellow-jackets-2d348',
    storageBucket: 'yellow-jackets-2d348.appspot.com',
    messagingSenderId: '169299527581'
  }
};
