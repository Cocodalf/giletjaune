# DOCUMENTATION

## Notes
On relancera un serveur OpenVidu pour la démo, on utilise un serveur de test pour le moment...
Pour le cloud => https://openvidu.io/docs/deployment/deploying-aws/

## Technos
Ionic
Angular
Firebase
OpenVidu

Ionic nous permet de créer rapidement une application mobile hybride
avec le framework Angular pour un dévellopement commun pour toute l'équipe.
Firebase nous fournis une base de donnée en temps réel ce qui nous fera gagner du temps pour une application de messagerie instantané
Openvidu nous fournis un serveur pour les conférence, peu facilement être déployé dans le cloud.

## pré requis
Docker
Node js

## Installation


## Structure
### Core
Module pour tout ce qui est utiliser par l'application
### Guest
Module pour tout ce qui est utiliser par un utilisateur non authentifié
### User
Module pour l'utilisateur authentifié